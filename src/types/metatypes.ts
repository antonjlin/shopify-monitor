export interface IShopifyMeta {
    id: number
    name: string
    city: string
    province: string
    country: string
    currency: string
    domain: string
    url: string
    myshopify_domain: string
    description: string | null
    ships_to_countries: string[] | null
    money_format: string | null
    published_collections_count: number
    published_products_count: number
}
