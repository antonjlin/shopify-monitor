import { IShopifyOption, IShopifyVariant } from '../types/shopifytypes'

export interface IProductJSONResponse {
    body: IProductJSONResponseBody
}

export interface IProductJSONResponseBody {
    products: IProductJsonProduct[]
}

export interface IProductJsonProduct {
    id: number
    title: string
    handle: string
    body_html: string | null
    published_at: Date
    created_at: Date
    updated_at: Date
    vendor: string | null
    product_type: string | null
    tags: any[] | null
    variants: IShopifyVariant[]
    images: any[] | null
    options: IShopifyOption[] | null
}
