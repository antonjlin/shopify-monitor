export interface BoostConfig {
    duration: number
    domain: string
    threadMultiplier: number
}
