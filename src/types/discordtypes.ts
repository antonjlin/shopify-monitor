export interface Author {
    icon_url: string
    name: string
    url: string
}

export interface Field {
    inline: boolean
    name: string
    value: string
}

export interface Footer {
    icon_url: string
    text: string
}

export interface Image {
    url: string
}

export interface Thumbnail {
    url: string
}

export interface Embed {
    title?: string
    type?: string
    description?: string
    url?: string
    timestamp?: string
    color?: string
    fields?: Field[]
    thumbnail?: Thumbnail
    image?: Image
    author?: Author
    footer?: Footer
}
