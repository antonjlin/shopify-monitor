export interface ISitemapProduct {
    loc: string
    changefreq: string
    lastmod?: string
}

export interface ISitemapResponse {
    urlset: ISitemapURLSet
}

export interface ISitemapURLSet {
    url: ISitemapProduct[]
    xmlns: string
    'xmlns: image': string
}
