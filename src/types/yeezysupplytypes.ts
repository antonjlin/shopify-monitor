export interface IYeezySupplyProdJson {
    handle: string
    products_count: number
    products: IYeezySupplyProduct[]
}

export interface IYeezySupplyProduct {
    id: number
    title: string
    handle: string
    tags: string[]
    i_440: string
    i_400: string
    i_340: string
    i_300: string
    i_220: string
    price: number
    url: string
    type: IYeezySupplyType
    available: boolean
    categories: IYeezySupplyCategory[]
    img?: string
    img_small?: string
    width?: number
}

export enum IYeezySupplyCategory {
    All = 'all',
    Tops = 'tops'
}

export enum IYeezySupplyType {
    Bottoms = 'BOTTOMS',
    Footwear = 'FOOTWEAR',
    Outerwear = 'OUTERWEAR',
    Tops = 'TOPS'
}
