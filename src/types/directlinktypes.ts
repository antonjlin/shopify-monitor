import { IShopifyOption, IShopifyVariant } from '../types/shopifytypes'

export interface IDirectLinkResponse {
    id: number
    title: string
    handle: string
    description: string
    published_at: Date
    created_at: Date
    vendor: string | null
    type: string | null
    tags: string[] | null
    price: number
    price_min: number | null
    price_max: number | null
    available: boolean
    price_varies: boolean | null
    compare_at_price: number | null
    compare_at_price_min: number | null
    compare_at_price_max: number | null
    compare_at_price_varies: boolean | null
    variants: IShopifyVariant[] | null
    images: string[] | null
    featured_image: string | null
    options: IShopifyOption[]
    url: string
}
