export interface LoggerLevel {
    name: string
    color: ChalkColor
    transports: LoggerTransports
}

export interface LoggerTransports {
    separateFile?: boolean
    combinedFile?: boolean
    console?: boolean
}

export interface LogMessage {
    message: string
    timestamp: string
    level: LoggerLevel
    tags: string[]
}

export type LogFunction = (message: any, extraTags: string[]) => void

export type LogMessageFormatter = (
    logMessage: LogMessage,
    colored: boolean
) => string

export type ChalkColor =
    | 'black'
    | 'red'
    | 'green'
    | 'yellow'
    | 'blue'
    | 'magenta'
    | 'cyan'
    | 'white'
    | 'gray'
    | 'redBright'
    | 'greenBright'
    | 'yellowBright'
    | 'blueBright'
    | 'magentaBright'
    | 'cyanBright'
    | 'whiteBright'
