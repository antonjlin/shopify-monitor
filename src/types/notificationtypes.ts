import { Product } from '../models/product'
import { Discount } from '../models/product'
import { DiscountEmbed } from '../notifications/discountembed'
import { PasswordEmbed } from '../notifications/passwordembed'
import { ProductEmbed } from '../notifications/productembed'
import { MonitorSource } from './basetypes'

export interface ProductNotification {
    product: Product
    source: MonitorSource
    keywordSet?: string
}

export interface PasswordNotification {
    isPasswordUp: boolean
    sendNotification: boolean
    source: MonitorSource
}

export interface DiscountNotification {
    discountInfo: Discount
    source: MonitorSource
}

export interface PotentialDropNotification {
    productNumDifference: number
    source: MonitorSource
    sendFiltered: boolean
}

export interface SubDomainNotification {
    subdomain: string
    source: MonitorSource
}

export interface Webhook {
    filtered: string[]
    unfiltered: string[]
    discounts: string[]
}

export interface GroupFormat {
    slack: boolean
    color: string
    logo: string
    footer: FooterFormat
    quicktasks: QuicktaskFormat
    botString?: (
        url: string,
        hyperlink: (s: string, url: string) => string
    ) => string
    productEmbedOverride?: (p: ProductEmbed) => ProductEmbed
    passwordEmbedOverride?: (p: PasswordEmbed) => PasswordEmbed
    discountEmbedOverride?: (p: DiscountEmbed) => DiscountEmbed
}

export interface QuicktaskFormat {
    includeQuicktasks: boolean
    getQTLink?: (url: string, botNum?: '1' | '2') => string
    configQTLink?: string
}

export interface FooterFormat {
    label: string
    includeMonitor: boolean
    includeTimestamp: boolean
    includeKeywords: boolean
}
