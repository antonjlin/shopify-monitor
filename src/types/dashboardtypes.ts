export type DashboardGroupName =
    | 'AIOMoji'
    | 'Calicos'
    | 'Cybersole'
    | 'FunkoGarage'
    | 'PopReport'
    | 'Calicos Funko'
    | 'Reselligent'
    | 'Balko'
    | 'Kodai'
    | 'Rihbey'

export interface DashboardGroup {
    _id: string
    group: DashboardGroupName
    enabled: boolean
    createdOn: string
    __v: number
}

export interface DashboardSite {
    meta: DashboardMeta
    monitor: DashboardMonitor
    extraMeta?: DashboardExtraMeta
    groups: DashboardGroupName[]
    productLinks: string[]
    _id?: string
    createdOn?: string
    __v?: number
    editedOn?: string
    useMyShopify: boolean
    subdomain: boolean
}

export interface DashboardExtraMeta {
    shipsTo?: DashboardShipsTo
    paypalOnly?: boolean
    accountRequired?: boolean
    siteUrl?: string
    id?: string
    city?: string
    province?: string
}

export interface DashboardShipsTo {
    US: boolean
    CA: boolean
    AU: boolean
    GB: boolean
}

export interface DashboardMeta {
    name: string
    country: string
    currency: string
    domain: string
    url: string
    myshopify_domain: string
    apiKey: string
}

export interface DashboardMonitor {
    atom: boolean
    json: boolean
    sitemap: boolean
}

export interface DashboardKeywords {
    _id: string
    keywords: string[]
    group: string
    createdOn: string
    __v: number
    editedOn?: string
    _keywords: any[]
}

export interface DashboardWebhook {
    filtered: string[]
    unfiltered: string[]
    discounts: string[]
    _id?: string
    group: DashboardGroupName
    createdOn?: string
    __v?: number
    editedOn?: string
}

export interface DashboardProxiesResponse {
    proxies: string[]
    _id: string
    group: DashboardGroupName
    createdOn: string
    __v: number
}
