export interface IShopifyVariant {
    id: number
    title: string
    option1: string | null
    option2: string | null
    option3: string | null
    sku: string | null
    requires_shipping: boolean
    taxable: boolean
    featured_image: string | null
    available: boolean
    name: string
    public_title: string
    options: string[] | null
    price: number
    weight: number | null
    compare_at_price: number | null
    inventory_management: string | null
    barcode: string | null
}

export interface IShopifyOption {
    name: string
    position: number
    values: string[]
}
