export interface IAtomProduct {
    id: string
    published: Date
    updated: Date
    link: IAtomLink
    title: string
    's:type': string
    's:vendor': string
    summary: IAtomSummary
    's:variant': IAtomVariant[]
}

export interface IAtomLink {
    rel: string
    type: string
    href: string
}

export interface IAtomSummary {
    '#text': string
    type: string
}

export interface IAtomVariant {
    id: string
    title: string
    's:price': IAtomPrice
    's:sku': string
    's:grams': number
}

export interface IAtomPrice {
    '#text': number
    currency: string
}
