// import EventEmitter from 'events'

// import { DiscordSender } from '../../../notifications/discordsender'
// import { ProductEmbed } from '../../../notifications/productembed'
// import { defaultLogger } from '../../../utils/logger'
// import { DiscordEmbed } from '../../../notifications/discordembed'
// import { Product } from '../../../models/product'

// const logger = defaultLogger('Notification Test')

// const unfilteredTestHook =
//     'https://discordapp.com/api/webhooks/550210214734069767/ZPzdpYliApnWsutM5gCfZqiPZ5JWcsnop017CV__Qq7x7ZxMpIgsF3V1kxmFWMxBwyZJ'

// let start = 0
// const numSuccessRepeats = 10
// const numErrorRepeats = 3
// const { errorCases, successCases } = getCases()

// const testEmitter = new EventEmitter()
// testEmitter.on('initialized', () => {
//     start = Date.now()
//     logger.info('Discord sender initialized')
// })
// testEmitter.on('finished', () =>
//     logger.info(
//         `Notification test for ${numSuccessRepeats *
//         successCases.length} successful notifications and ${numErrorRepeats *
//         errorCases.length} error notifications took ${(Date.now() - start) /
//         1000} seconds`
//     )
// )

// const discordSender = new DiscordSender(unfilteredTestHook, false, testEmitter)
// runCases(discordSender, numErrorRepeats, errorCases)
// runCases(discordSender, numSuccessRepeats, successCases)

// function runCases(
//     discordSender: DiscordSender,
//     numRepeats: number,
//     cases: DiscordEmbed[]
// ): void {
//     for (let i = 0; i < numRepeats; i++) {
//         cases.forEach(embed => {
//             discordSender.add(embed)
//         })
//     }
// }

// // All test cases below
// function getCases() {
//     return {
//         errorCases: [new DiscordEmbed().setURL('invalid!')],
//         successCases: [
//             new ProductEmbed({
//                 product: {
//                     title:
//                         'Gilbert & George/Supreme DEATH AFTER LIFE Skateboard - 8.625" x 32.375"',
//                     link: 'https://www.supremenewyork.com/shop/172218',
//                     id: '23291',
//                     price: Product.normalizePrice(Number(88), 'USD'),
//                     image:
//                         'http://assets.supremenewyork.com/171070/sm/3c-mUXaYRr8.jpg',
//                     variants: [
//                         {
//                             title: '8 5/8',
//                             id: '65812',
//                             available: false
//                         }
//                     ]
//                 },
//                 source: {
//                     endpoint:
//                         'https://www.supremenewyork.com/mobile_stock.json',
//                     site: {
//                         displayName: 'Supreme 0',
//                         domain: 'www.supremenewyork.com',
//                         url: 'http://www.supremenewyork.com',
//                         currency: 'USD'
//                     },
//                     monitor: 'SitemapMonitor'
//                 }
//             }),
//             new ProductEmbed({
//                 product: {
//                     title: 'Supreme®/Nike® Air Tailwind IV - White',
//                     link: 'https://www.supremenewyork.com/shop/172205',
//                     id: '23254',
//                     price: Product.normalizePrice(Number(88), 'USD'),
//                     image:
//                         'http://assets.supremenewyork.com/171001/sm/EDB_FGsq-fo.jpg',
//                     variants: [
//                         {
//                             title: '8',
//                             id: '65694',
//                             available: false
//                         },
//                         {
//                             title: '8 1/2',
//                             id: '65695',
//                             available: false
//                         },
//                         {
//                             title: '9',
//                             id: '65696',
//                             available: false
//                         },
//                         {
//                             title: '9 1/2',
//                             id: '65697',
//                             available: false
//                         },
//                         {
//                             title: '10',
//                             id: '65698',
//                             available: false
//                         },
//                         {
//                             title: '10.5',
//                             id: '65699',
//                             available: false
//                         },
//                         {
//                             title: '11',
//                             id: '65700',
//                             available: false
//                         },
//                         {
//                             title: '11.5',
//                             id: '65701',
//                             available: false
//                         },
//                         {
//                             title: '12',
//                             id: '65702',
//                             available: false
//                         },
//                         {
//                             title: '13',
//                             id: '65703',
//                             available: false
//                         }
//                     ]
//                 },
//                 source: {
//                     endpoint:
//                         'https://www.supremenewyork.com/mobile_stock.json',
//                     site: {
//                         displayName: 'Supreme 0',
//                         domain: 'www.supremenewyork.com',
//                         url: 'http://www.supremenewyork.com',
//                         currency: 'USD'
//                     },
//                     monitor: 'AtomMonitor'
//                 }
//             }),
//             new ProductEmbed({
//                 product: {
//                     title: 'Yeezy Boost 700 V2',
//                     link: 'https://frenzy.sale/77aa1e1d3a8b',
//                     id: '2033',
//                     price: Product.normalizePrice(Number(300), 'USD'),
//                     image:
//                         'https://cdn.shopify.com/s/files/1/0946/3304/' +
//                         'products/2019-03-18_Yeezy_Boost_700_V2_Geode_SKU-_EG6860.jpg?v=1553097544',
//                     releaseTime: '2019-03-23',
//                     available: true
//                 },
//                 source: {
//                     endpoint: 'https://frenzy.shopifyapps.com/api/flashsales',
//                     site: {
//                         displayName: 'Frenzy',
//                         domain: 'frenzy.sale',
//                         url: 'https://frenzy.sale',
//                         currency: 'USD'
//                     },
//                     monitor: 'ProductsJSONMonitor'
//                 }
//             })
//         ]
//     }
// }
