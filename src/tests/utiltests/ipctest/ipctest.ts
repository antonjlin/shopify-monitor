import { IPCClient } from '../../../utils/ipcclient'
import { IPCServer } from '../../../utils/ipcserver'

const ipcServer = new IPCServer().on('ping', (_: any, socket: any) => {
    ipcServer.logger.info('🏓  PING')
    ipcServer.emit(socket, 'pong')
})

const ipcClient = new IPCClient('server')
    .registerListener('connect', () => {
        ipcClient.logger.success(
            `Connected to IPC server: ${ipcClient.serverName}`
        )
        ipcClient.emit('ping')
    })
    .registerListener('pong', () => {
        ipcClient.logger.success('PONG 🏓')
        setTimeout(() => {
            ipcClient.emit('ping')
        }, 5000)
    })
