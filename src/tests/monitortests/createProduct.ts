require('dotenv').config()

import Shopify from 'shopify-api-node'

const shopify = new Shopify({
    shopName: 'calicosiotest',
    apiKey: process.env.SHOPIFY_API_KEY || '',
    password: process.env.SHOPIFY_API_PASS || ''
})

const variant = (name: string, price: string) => {
    return {
        price,
        option1: name,
        sku: '123',
        inventory_management: 'shopify',
        inventory_quantity: 10
    }
}

export const createProduct = (title: string, price: string = '69.00') => {
    shopify.product.create({
        title,
        variants: [
            variant('This', price),
            variant('is', price),
            variant('a', price),
            variant('test', price),
            variant('product', price)
        ]
    })
}
