// import { EventEmitter } from 'events'

// import { sites, proxies } from './data'
// import { defaultLogger } from '../../../utils/logger'
// import { App } from '../../../app'
// import { createProduct } from '../createProduct'

// const logger = defaultLogger('Atom Test')

// export async function atomTest(eventEmitter: EventEmitter) {
//     const app = new App(
//         { testMode: true, sendInitialScrape: false, silent: true },
//         sites,
//         proxies
//     )
//     app.start()
//     logger.init('Launching product in 2s')

//     await new Promise((resolve, reject) => setTimeout(resolve, 2000))

//     const title = `ATOM Test Product ${Date.now()}`
//     createProduct(title)
//     logger.test(`Created test product ${title}`)

//     await new Promise((resolve, reject) => setTimeout(resolve, 5000))
//     app.stop()
//     await new Promise((resolve, reject) => setTimeout(resolve, 2000))

//     logger.test('Test completed')
//     eventEmitter.emit('complete')
// }
