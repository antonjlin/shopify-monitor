import { Discount, Product } from '../models/product'

// % discount needed to send notification
const DISCOUNT_THRESHOLD = 25

export class ProductListings {
    constructor(public products: Product[] = []) {}

    public clear() {
        this.products = []
    }

    public getRestockedProducts(products: Product[]): Product[] {
        const restockedProducts: Product[] = []
        products.forEach((product: Product) => {
            const matchedProduct = this.findProductByKey(product, 'id')
            if (
                matchedProduct === undefined ||
                this.isRestock(product, matchedProduct)
            ) {
                restockedProducts.push(product)
            } else {
                // old products
            }
        })
        this.products = products
        return restockedProducts
    }

    public isRestock(newProd: Product, oldProd: Product): boolean {
        // TODO: currently only does new products, make it do restocks & iterate to variants
        let isRestock: null | boolean = null
        if (oldProd.title !== newProd.title) {
            return true
        }
        if (newProd.variants) {
            for (const variant of newProd.variants) {
                if (variant.available) {
                    if (
                        oldProd.variantMap &&
                        (oldProd.variantMap[variant.id] === undefined ||
                            !oldProd.variantMap[variant.id].available)
                    ) {
                        isRestock = true
                    }
                }
            }
            if (isRestock === null) {
                isRestock = false
            }
        }
        if (isRestock === null) {
            return Boolean(!oldProd.available && newProd.available)
        }
        return isRestock
    }

    public getDiscountedProducts(products: Product[]): Discount[] {
        if (products.length === 0 || !products[0].price) { return [] }
        const discountedProducts: Discount[] = []
        products.forEach((product: Product) => {
            const matchedProduct = this.findProductByKey(product, 'id')
            if (matchedProduct !== undefined) {
                const discountInfo = this.checkDiscount(product, matchedProduct)
                if (discountInfo !== undefined) {
                    discountedProducts.push(discountInfo)
                } else {
                    // not discounted
                }
            }
        })
        return discountedProducts
    }

    public checkDiscount(newProd: Product, oldProd: Product): Discount | undefined {
        // newProd.price!.priceUSD (! symbol) was not working
        if (!newProd.price || !oldProd.price) { return undefined }
        const newPrice = newProd.price!.usd
        const originalPrice = oldProd.price!.usd
        if (originalPrice === 0) { return undefined }
        const percentDiscount = (1 - newPrice / originalPrice) * 100
        if (percentDiscount >= DISCOUNT_THRESHOLD) {
            return {
                product: newProd,
                originalPrice,
                newPrice,
                percentDiscount
            }
        } else {
            return undefined
        }
    }

    public findProductByKey(product: Product, key: keyof Product): Product {
        return this.products.filter(
            (oldProduct) => oldProduct[key] === product[key]
        )[0]
    }
}
