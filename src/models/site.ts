import { DashboardExtraMeta, DashboardSite } from '../types/dashboardtypes'

export interface Site {
    displayName: string
    domain: string
    url: string
    currency: string
    groups: string[]
    extraMeta?: DashboardExtraMeta
}

export class Site {

    public static fromDashboard(
        site: DashboardSite,
        useMyShopifyDomain: boolean = false
    ): Site {
        const { meta, groups, extraMeta } = site
        const { name, domain, url, currency, myshopify_domain } = meta
        if (useMyShopifyDomain) {
            return new Site({
                domain: myshopify_domain,
                url,
                currency,
                groups,
                displayName: name,
                extraMeta
            })
        }
        return new Site({
            domain,
            url,
            currency,
            groups,
            displayName: name,
            extraMeta
        })
    }
    constructor(args: Site) {
        return Object.assign(this, args)
    }
}
