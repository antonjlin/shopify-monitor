import { conversions } from '../data/currencies'
import { Currency } from '../types/basetypes'

export interface Product {
    title: string
    link: string
    id: string

    price?: Price
    image?: string
    sku?: string
    variants?: Variant[]
    variantMap?: VariantMap
    available?: boolean
    releaseTime?: string
    releaseMethod?: string
    deepLink?: string
    tags?: string[]
    vitalOrb?: boolean
}

export interface Price {
    priceStr: string
    usd: number
    original: number
    currency: string
}

export interface Variant {
    title: string
    available: boolean
    id: string
    atcLink: string

    price?: string
    stock?: number
}

export interface Discount {
    product: Product
    originalPrice: number
    newPrice: number
    percentDiscount: number
}

export interface VariantMap {
    [key: string]: Variant
}

export class Product {

    public static normalizePrice(
        prodPrice: number,
        curr: string,
        convertToDollars: boolean = false
    ): Price {
        const currency = curr as Currency
        const price = convertToDollars ? (prodPrice * 1) / 100 : prodPrice
        const priceUSD = conversions[currency]
            ? price * conversions[currency]
            : price
        const priceStr =
            currency === 'USD'
                ? `$${Number(priceUSD).toFixed(2)}`
                : `${price} ${currency} ($${Number(priceUSD).toFixed(2)})`
        return {
            priceStr,
            original: price,
            usd: priceUSD,
            currency: curr
        }
    }

    public static getATCLink(url: string, variantID: string): string {
        return `${url}/cart/${variantID}:1`
    }
    constructor(args: Product) {
        const variantMap: VariantMap = args.variants
            ? args.variants.reduce((acc: VariantMap, cur) => {
                  acc[cur.id] = cur
                  return acc
              }, {})
            : {}
        return Object.assign(this, args, { variantMap })
    }
}
