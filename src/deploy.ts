import bodyParser from 'body-parser'
import express from 'express'
import { argv } from 'yargs'
const simpleGit = require('simple-git/promise')()
import * as cp from 'child_process'
import * as os from 'os'
import shell from 'shelljs'

import { BoostConfig } from './types/boosttypes'
import { IPCClient } from './utils/ipcclient'
import { defaultLogger } from './utils/logger'
import { postGitUpdate } from './utils/sendGithubNotification'

const testMode = Boolean(argv.test)
const port = 9000

let app: cp.ChildProcess
let ipc: IPCClient

const logger = defaultLogger('Deploy')

const launchApp = (delay = 3000) => {
    const launch = () => {
        app = cp.fork('./build/app.js', [String(testMode)])
        ipc = new IPCClient('server')
        ipc.registerListener('connect', () => {
            logger.ipc('Server connected to IPC')
        })
        logger.info('Started app')
    }

    const { stdout } = shell.exec('ps -aux | grep ./build/monitors | wc -l', {
        silent: false
    })
    const monitorsRunning = Number(stdout)

    if (monitorsRunning <= 2) {
        logger.success(`${monitorsRunning} processes running. Launching app`)
        launch()
    } else {
        logger.warn(
            `${monitorsRunning} processes running. Launching app after delay`
        )
        setTimeout(() => launchApp(delay), delay)
    }
}

const killApp = () => {
    app && app.kill('SIGINT')
    logger.info('Killed app')
}

const productionServer = () => {
    const app = express()
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))

    app.all('/restart', (req, res) => {
        res.json({ status: 'Request received' })
        killApp()
        launchApp()
        postGitUpdate('Dashboard updated')
    })

    app.all('/boost', (req, res) => {
        const boostConfig: BoostConfig = req.body
        logger.boost('Received BOOST message from dashboard')
        ipc.emit('boost', boostConfig)
        res.json({ status: 'Request received' })
    })

    app.post('/payload', (req, res) => {
        res.json({ status: 'Push received' })
        logger.info('GIT PUSH DETECTED')
        const branch = req.body.ref
        const changelogString = req.body.commits
            ? req.body.commits
                  .map(
                      (x: any) =>
                          `<${x.url}|${x.message}> - ${x.committer.username}`
                  )
                  .join('\n')
            : ''
        logger.info('Git push from branch ' + branch)

        if (branch && branch.includes('master')) {
            logger.info('MASTER BRANCH DETECTED')
            simpleGit
                .pull('origin', 'master', { '--no-rebase': null })
                .then(() => {
                    logger.info('Pulling from git...')
                    shell.exec('git pull')
                    logger.info('Compiling typescript...')
                    shell.exec('tsc -p tsconfig.json --watch false')
                    logger.info('Installing dependencies...')
                    shell.exec('npm install')
                    logger.info('Auditing dependencies...')
                    shell.exec('npm audit fix')
                    logger.success('App ready for deployment')
                    killApp()
                    launchApp()
                    postGitUpdate(changelogString)
                })
                .catch((err: any) => console.log(err))
        }
    })

    app.listen(port, () => {
        launchApp()
        logger.info(`App running on port ${port}`)
    })
}

if (testMode) {
    productionServer()
} else {
    productionServer()
}
