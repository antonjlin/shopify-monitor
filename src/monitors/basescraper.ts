import rp from 'request-promise'
import uuid from 'uuid'

import { Discount, Product } from '../models/product'
import { MonitorSource, TestConfig } from '../types/basetypes'
import { ProductNotification } from '../types/notificationtypes'
import { DatadogSender } from '../utils/datadogsender'
import { IPCClient } from '../utils/ipcclient'
import { defaultLogger } from '../utils/logger'
import { ProxyManager } from '../utils/proxymanager'
import { Timer } from '../utils/timer'
import { UserAgentGenerator } from '../utils/useragentgenerator'

export class BaseScraper {
    public proxyManager: ProxyManager

    public testConfig: TestConfig = {
        testMode: false,
        sendInitialScrape: false,
        silent: false
    }
    public timer = new Timer()
    public ipcConnected = false
    public stopped = false
    public logger = defaultLogger(this.constructor.name, false, './logs')
    public ipc = new IPCClient('server')
    public userAgentGenerator = new UserAgentGenerator()
    public scrapeRestockLink = false
    public datadogSender = new DatadogSender()

    constructor(
        public proxies: string[],
        public delay: number,
        public timeout: number
    ) {
        this.proxyManager = new ProxyManager(proxies)
    }

    public async start(testConfig: TestConfig = this.testConfig) {
        this.testConfig = testConfig
        this.logger = defaultLogger(this.logger.tags, this.testConfig.silent)
        await this.registerIPCClient()
        this.stopped = false
        this.timer.reset()
        this.run()
    }

    public stop() {
        this.stopped = true
    }

    public async run() {
        throw new Error('Method not implemented.')
    }

    public getOptions(url: string, overrideParams: object): rp.Options {
        const jar = rp.jar()
        jar.setCookie(`${uuid()}=`, url)
        return Object.assign(
            {
                url,
                jar,
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    'User-Agent': this.userAgentGenerator.get()
                },
                timeout: this.timeout,
                time: true,
                gzip: true,
                json: true,
                rejectUnauthorized: false,
                resolveWithFullResponse: true,
                proxy: this.proxyManager.get()
                // tunnel: false,
                // strictSSL: false
            },
            overrideParams
        )
    }

    public async registerIPCClient(): Promise<void | {}> {
        throw new Error('Method not implemented.')
    }

    public sendNotifications(products: Product[], source: MonitorSource) {
        if (products.length && !this.ipcConnected) {
            this.logger.error(
                'Trying to send restock notification while IPC is not yet connected, retrying in 1s...'
            )
            setTimeout(() => this.sendNotifications(products, source), 1000)
        } else {
            products.forEach((product) => {
                const message: ProductNotification = {
                    product,
                    source
                }
                this.ipc.emit('restock', message)
                this.scrapeRestockLink &&
                    this.ipc.emit('scrape.link', {
                        link: product.link,
                        site: source.site
                    })
            })
        }
    }

    public sendPasswordNotification(
        isPasswordUp: boolean,
        sendNotification: boolean,
        source: MonitorSource
    ) {
        if (sendNotification) {
            this.datadogSender.passwordEvent(isPasswordUp, source)
        }
        if (!this.ipcConnected) {
            this.logger.error(
                'Trying to send password notification while IPC is not yet connected, retrying in 1s...'
            )
            setTimeout(
                () =>
                    this.sendPasswordNotification(
                        isPasswordUp,
                        sendNotification,
                        source
                    ),
                1000
            )
        } else {
            this.ipc.emit('password.update', {
                isPasswordUp,
                source,
                sendNotification
            })
        }
    }

    public sendDiscountNotifications(discounts: Discount[], source: MonitorSource) {
        if (discounts.length && !this.ipcConnected) {
            this.logger.error(
                'Trying to send discount notification while IPC is not yet connected, retrying in 1s...'
            )
            setTimeout(
                () => this.sendDiscountNotifications(discounts, source),
                1000
            )
        } else {
            discounts.forEach((discount) => {
                this.ipc.emit('discount.update', {
                    discountInfo: discount,
                    source
                })
            })
        }
    }
}
