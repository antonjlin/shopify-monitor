import crypto from 'crypto'
import * as xmlParser from 'fast-xml-parser'
import rp from 'request-promise'

import { Product } from '../models/product'
import { Site } from '../models/site'
import { IAtomProduct } from '../types/atomtypes'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class AtomMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[]) {
        const endpoint = `https://${site.domain}/collections/all.atom`
        super(site, endpoint, proxies)
        this.scrapeRestockLink = true
    }

    public getOptions() {
        return super.getOptions({
            json: false,
            qs: {
                limit: crypto.randomBytes(64).toString('hex')
            }
        })
    }

    public async getResponse(options: rp.Options) {
        return rpt(options)
    }

    public parseResponse(res: any): Product[] {
        const atomProducts: IAtomProduct[] = xmlParser.parse(res.body, {
            ignoreAttributes: false,
            attributeNamePrefix: ''
        }).feed.entry
        if (!atomProducts) {
            return []
        } else if (!Array.isArray(atomProducts)) {
            return this.parseProducts([atomProducts])
        }
        return this.parseProducts(atomProducts)
    }

    public parseProducts(atomProducts: IAtomProduct[]): Product[] {
        return atomProducts.map(this.parseProduct)
    }

    public parseProduct(atomProduct: IAtomProduct): Product {
        const { title, link, id } = atomProduct
        return new Product({
            title,
            id: id.split('products/')[1].split('</id>')[0],
            link: link.href
        })
    }
}

startMonitor(AtomMonitor, process.argv.slice(2))
