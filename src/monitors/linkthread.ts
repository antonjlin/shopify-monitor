import { Product, Variant } from '../models/product'
import { Site } from '../models/site'
import { TestConfig } from '../types/basetypes'
import { IDirectLinkResponse } from '../types/directlinktypes'
import { IShopifyVariant } from '../types/shopifytypes'
import { ProxyManager } from '../utils/proxymanager'
import { rpt } from '../utils/requestpromisetimeout'
import { BaseScraper } from './basescraper'

export class LinkThread extends BaseScraper {
    constructor(proxies: string[]) {
        super(proxies, 0, 30000)
    }

    public async run() { }

    public async registerIPCClient(): Promise<void | {}> {
        return new Promise((resolve, _) => {
            this.ipc
                .registerListener('connect', () => {
                    this.ipcConnected = true
                    this.ipc.emit('link.thread.connected')
                })
                .registerListener('scrape.link', (data: any) => {
                    const { link, site } = data
                    this.logger.info(`Scraping link: ${link}`)
                    this.scrapeLink(link, site)
                })
            resolve()
        })
    }

    public async scrapeLink(link: string, site: Site, tries = 0) {
        if (tries > 10) {
            this.logger.error(
                `Tried to scrape ${link} ${tries} times, giving up...`
            )
            return
        }
        try {
            const [products] = await Promise.all([this.getProducts(link, site)])
            this.sendNotifications(products, {
                site,
                endpoint: link,
                monitor: this.constructor.name,
                timeout: this.timeout,
                delay: this.delay
            })
        } catch (err) {
            setTimeout(() => this.scrapeLink(link, site, tries + 1), 0)
            if (
                err.statusCode === 430 ||
                (err.message &&
                    err.message.includes('MANUAL_TIMEOUT') ||
                    err.message.includes('TIMEDOUT') ||
                    err.message.includes(
                        'tunneling socket could not be established'
                    ) ||
                    err.message.includes('socket hang up')
                )
            ) {
                // do nothing
            } else {
                this.logger.error(err)
            }
        }
    }

    public async getProducts(link: string, site: Site): Promise<Product[]> {
        const options = this.getOptions(link, {
            qs: {
                format: 'js'
                // REDACTED CACHE BYPASS
            }
        })
        try {
            const res = await rpt(options)
            const products: Product[] = this.parseResponseBody(
                res.body,
                link,
                site
            )
            this.datadogSender.logRequestSuccess(
                res.statusCode,
                res.elapsedTime,
                {
                    site,
                    endpoint: link,
                    monitor: this.constructor.name,
                    timeout: this.timeout,
                    delay: this.delay
                }
            )
            return products
        } catch (err) {
            throw err
        }
    }

    public parseResponseBody(
        body: IDirectLinkResponse,
        link: string,
        site: Site
    ): Product[] {
        const { title, id, price, variants, images, tags } = body
        return [
            new Product({
                title,
                link,
                tags: tags || [],
                id: id.toString(),
                price: Product.normalizePrice(
                    Number(price),
                    site.currency,
                    true
                ),
                ...(variants && {
                    variants: variants.map((variant) =>
                        this.parseVariant(variant, site)
                    )
                }),
                ...(images && { image: images[0] })
            })
        ]
    }

    public parseVariant(variant: IShopifyVariant, site: Site): Variant {
        const { title, id, available } = variant
        return {
            title,
            id: id.toString(),
            available,
            atcLink: Product.getATCLink(site.url, id.toString())
        }
    }
}

const args = process.argv.slice(2)
let testConfig: TestConfig
try {
    testConfig = JSON.parse(args[0])
    if (
        testConfig.sendInitialScrape === undefined ||
        testConfig.testMode === undefined ||
        testConfig.silent === undefined
    ) {
        throw new Error()
    }
} catch (err) {
    testConfig = {
        testMode: false,
        sendInitialScrape: false,
        silent: false
    }
}

const proxies = JSON.parse(process.argv.slice(2)[0])

if (
    !Array.isArray(proxies) ||
    (proxies.length && typeof proxies[0] !== 'string')
) {
    throw new Error('No valid proxies passed in to LinkThread')
}
const linkThread = new LinkThread(proxies)
linkThread.start(testConfig)
