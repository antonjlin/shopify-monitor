import { Site } from '../models/site'
import { startMonitor } from '../utils/startMonitor'
import { HTMLMonitor } from './htmlmonitor'

export class MondayProgramMonitor extends HTMLMonitor {
    constructor(site: Site, proxies: string[]) {
        super(site, proxies)
        this.endpoint = `https://${site.domain}/collections/kith-monday-program`

    }
}

startMonitor(MondayProgramMonitor, process.argv.slice(2))
