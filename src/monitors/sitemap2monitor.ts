import { Site } from '../models/site'
import { startMonitor } from '../utils/startMonitor'
import { SitemapMonitor } from './sitemapmonitor'

export class Sitemap2Monitor extends SitemapMonitor {
    constructor(site: Site, proxies: string[]) {
        super(site, proxies)
        this.endpoint = `https://${site.domain}/sitemap_products_2.xml`
    }
}

startMonitor(Sitemap2Monitor, process.argv.slice(2))
