import rp from 'request-promise'

import { Site } from '../models/site'
import { MonitorSource } from '../types/basetypes'
import { DatadogSender } from '../utils/datadogsender'
import { IPCClient } from '../utils/ipcclient'
import { defaultLogger } from '../utils/logger'
import { ProxyManager } from '../utils/proxymanager'
import { UserAgentGenerator } from '../utils/useragentgenerator'

interface SubDomainInstances {
    source: MonitorSource
    currentDomains: string[]
    initialScrape: boolean
}

export interface DomainInfo {
    issuer_ca_id: number
    issuer_name: string
    name_value: string
    min_cert_id: number
    min_entry_timestamp: Date
    not_before: Date
    not_after: Date
}

export class SubDomainMonitor {
    public runningInstances: SubDomainInstances[] = []

    public delay: number = 15000
    public timeout: number = 7500

    public userAgentGenerator = new UserAgentGenerator()
    public datadogSender = new DatadogSender()
    public proxyManager = new ProxyManager(['localhost'])
    public logger = defaultLogger(this.constructor.name, false, './logs')
    public ipc = new IPCClient('server')

    constructor(sites: Site[]) {
        sites.forEach((s: Site) => {
            this.addMonitor(s)
        })
        this.run()
    }

    public async registerIPCClient(): Promise<void | {}> {
        return new Promise((resolve, _) => {
            this.ipc
                .registerListener('connect', () => {
                    this.ipc.emit('subdomain.monitor.connected')
                    this.logger.info('SubDomain Monitor Connected.')
                })
                .registerListener('proxy', (data: any) => {
                    this.proxyManager = new ProxyManager(data)
                })
            resolve()
        })
    }

    public async start() {
        await this.registerIPCClient()
        this.run()
    }

    public async run() {
        this.runningInstances.forEach((instance: SubDomainInstances) => {
            this.compareInfo(instance)
        })

        setTimeout(() => {
            this.run()
        }, this.delay)
    }

    public async getInfo(instance: SubDomainInstances): Promise<DomainInfo[]> {
        const options = {
            url: instance.source.endpoint,
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'User-Agent': this.userAgentGenerator.get()
            },
            timeout: this.timeout,
            time: true,
            gzip: true,
            json: true,
            rejectUnauthorized: false,
            resolveWithFullResponse: true,
            proxy: this.proxyManager.get()
        }
        try {
            const res = await rp(options)
            this.datadogSender.logRequestSuccess(
                res.statusCode,
                res.elapsedTime,
                instance.source
            )
            return res.body
        } catch (err) {
            throw err
        }
    }

    public async getAllSubDomains(instance: SubDomainInstances): Promise<string[]> {
        const info: DomainInfo[] = await this.getInfo(instance)
        return info.map((i: DomainInfo) => {
            return i.name_value.replace('*.', '')
        })
    }

    public async compareInfo(instance: SubDomainInstances) {
        try {
            if (instance.initialScrape) {
                instance.currentDomains = await this.getAllSubDomains(instance)
                instance.initialScrape = false
            } else {
                const newDomains: string[] = await this.getAllSubDomains(instance)
                newDomains.forEach((domain: string) => {
                    if (
                        instance.currentDomains.indexOf(domain) === -1
                    ) {
                        this.sendSubDomainNotification(domain, instance)
                    }
                })
                instance.currentDomains = newDomains
            }
        } catch (err) {
            this.logger.error(err)
        }
    }

    public sendSubDomainNotification(subdomain: string, instance: SubDomainInstances) {
        console.log(subdomain)
        this.ipc.emit('subdomain.update', {
            subdomain,
            source: instance.source
        })
    }

    public addMonitor(site: Site) {
        if (
            site.domain.indexOf('myshopify') === -1
        ) {
            const mainlink = site.domain
                .replace('*.', '')
                .split('.')
                .slice(-2).join('.')
                .replace('/', '')
            site.url = mainlink
            const source = {
                endpoint: `https://crt.sh/json?q=%25${mainlink}`,
                delay: this.delay,
                timeout: this.timeout,
                site,
                monitor: this.constructor.name
            }
            this.runningInstances.push({
                source,
                currentDomains: [],
                initialScrape: true
            })
        } else {
            this.logger.error('Cannot add .myshopify domains to subdomain monitor.')
        }
    }
}
const sites = process.argv.slice(2)[0]
const subdomainMonitor = new SubDomainMonitor(JSON.parse(sites) as Site[])
subdomainMonitor.start()

/*
    TEST
*/

// const site: Site = {
//     displayName: 'calicosiotest',
//     domain: 'calicos.io',
//     url: 'idfk',
//     currency: 'USD',
//     groups: ['Calicos']
// }
// subdomainMonitor.addMonitor(site)
