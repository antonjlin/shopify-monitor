import cheerio from 'cheerio'
import randomInt from 'random-int'
import rp from 'request-promise'

import { Product } from '../models/product'
import { Site } from '../models/site'
import {
    IYeezySupplyProdJson as IYeezySupplyProdJSON,
    IYeezySupplyProduct,
} from '../types/yeezysupplytypes'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class YeezySupplyMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[]) {
        super(site, `https://${site.domain}`, proxies)
        this.scrapeRestockLink = true
    }

    public getOptions() {
        return super.getOptions({
            qs: {
                page: randomInt(10000000, 100000000)
            }
        })
    }

    public async getResponse(options: rp.Options) {
        return rpt(options)
    }

    public toJSON(res: any) {
        return this.parseHTML(res.body)
    }

    public parseResponse(res: any): Product[] {
        const productJSON = this.parseHTML(res.body)
        if (productJSON) {
            return productJSON.products.map(this.parseProduct.bind(this))
        }
        return []
    }

    public parseHTML(html: string): IYeezySupplyProdJSON | null {
        const $ = cheerio.load(html)
        const htmlProducts: string | null = $(
            $('#main')
                .find($('script#js-featured-json'))
                .get(0)
        ).html()

        const productJSON: IYeezySupplyProdJSON | null = htmlProducts
            ? JSON.parse(htmlProducts)
            : null
        return productJSON
    }

    public parseProduct(product: IYeezySupplyProduct): Product {
        return new Product({
            title: product.title.replace('\n', ''),
            link: `${this.site.url}${product.url}`,
            id: product.id.toString(),
            available: product.available,
            price: Product.normalizePrice(
                Number(product.price),
                this.site.currency,
                true
            ),
            tags: product.tags,
            image: `https:${product.i_440.replace('/', '/')}`
        })
    }
}

startMonitor(YeezySupplyMonitor, process.argv.slice(2))
