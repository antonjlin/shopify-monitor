import rp from 'request-promise'
import { Product } from '../models/product'
import { Site } from '../models/site'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class HTMLMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[]) {
        const endpoint = `https://${
            site.domain
            }/collections/sneakers/final-sale`
        super(site, endpoint, proxies)
        this.scrapeRestockLink = true
    }

    public getOptions() {
        return super.getOptions({
            qs: {
                sort_by: 'created-descending'
            },
            json: false,
            headers: {
                'content-type': 'application/json',
                'User-Agent': this.userAgentGenerator.get(),
                // REDACTED CACHE BYPASS
            }
        })
    }

    public async getResponse(options: rp.Options) {
        try {
            const res = rpt(options)
            return res
        } catch (err) {
            if (err.statusCode === 404) {
                this.updatePasswordPage(true)
            }
            throw err
        }
    }

    public parseResponse(res: any): Product[] {
        const handles: string[] = this.parseHTML(res.body)
        // console.log(handles)
        return handles.map((x) => {
            return this.parseProduct(x)
        })
    }

    public parseHTML(body: string): string[] {
        const links = body.match(
            /(\/products\/(?:[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*))/g
        )
        if (!links) { return [] }

        return links
            .filter((item, index) => {
                return links!.indexOf(item) >= index // remove duplicates from array
            })
            .filter((x) => x.replace('/products/', '').length > 0)
            .filter(
                (x) =>
                    !x.includes('.jpeg') &&
                    !x.includes('.jpg') &&
                    !x.includes('.png')
            )
    }

    public parseProduct(handle: string): Product {
        const j = new Product({
            title: titleCase(
                handle.replace('/products/', '').replace(/-/gi, ' ')
            ),
            link: `https://${this.site.domain}${handle}`,
            id: handle
        })
        return j
    }
}

function titleCase(str: string) {
    return str.toLowerCase().replace(/\b(\w)/g, (s) => s.toUpperCase())
}

// only run the monitor if this file is not being imported
if (module.parent === null) {
    startMonitor(HTMLMonitor, process.argv.slice(2))
}
