import rp from 'request-promise'

import { Product } from '../models/product'
import { Site } from '../models/site'
import { IDirectLinkResponse } from '../types/directlinktypes'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class DirectLinkMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[], link: string) {
        super(site, link, proxies)
    }
    public getOptions() {
        return super.getOptions({
            qs: {
                format: 'js'
                // REDACTED CACHE BYPASS
            }
        })
    }

    public async getResponse(options: rp.Options) {
        try {
            const res = await rpt(options)
            return res
        } catch (err) {
            // early link support
            if (err.statusCode === 404) {
                this.isFirstScrape = false
                throw { message: 'EARLY_LINK', statusCode: 123 }
            }
            throw err
        }
    }

    public parseResponse({ body }: { body: IDirectLinkResponse }): Product[] {
        const { title, id, price, variants, images, tags } = body
        if (title === undefined) {
            this.isLinkBroken || this.logger.error('Link is broken')
            this.isLinkBroken = true
            return []
        }
        this.isLinkBroken = false
        return [
            new Product({
                title,
                tags: tags || [],
                link: this.endpoint,
                id: id.toString(),
                price: Product.normalizePrice(
                    Number(price),
                    this.site.currency,
                    true
                ),
                ...(variants && {
                    variants: variants.map((variant) => {
                        const { title, id, available } = variant
                        return {
                            title,
                            id: id.toString(),
                            available,
                            atcLink: Product.getATCLink(
                                this.site.url,
                                id.toString()
                            )
                        }
                    })
                }),
                ...(images && { image: images[0] })
            })
        ]
    }
}

startMonitor(DirectLinkMonitor, process.argv.slice(2))
