import * as xmlParser from 'fast-xml-parser'
import randomInt from 'random-int'
import md5 from 'md5'
import rp from 'request-promise'

import { Product } from '../models/product'
import { Site } from '../models/site'
import { ISitemapProduct } from '../types/sitemaptypes'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class SitemapMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[]) {
        const endpoint = `https://${site.domain}/sitemap_products_1.xml`
        super(site, endpoint, proxies)
        this.scrapeRestockLink = true
    }

    public getOptions() {
        const jar = rp.jar()
        jar.setCookie('storefront_digest=', md5(new Date().getMilliseconds().toString()))
        return super.getOptions({
            jar,
            qs: {
                from: randomInt(1000000, 10000000),
                to: randomInt(10000000000000000, 1000000000000000000)
            },
            json: false,
            headers: {
                'content-type': 'application/json',
                'User-Agent': this.userAgentGenerator.get(),
                'x-shopify-api-features': Math.round(Math.random() * 10000000)
            }
        })
    }

    public async getResponse(options: rp.Options) {
        try {
            const res = rpt(options)
            return res
        } catch (err) {
            if (err.statusCode === 404) {
                this.updatePasswordPage(true)
            }
            throw err
        }
    }

    public parseResponse(res: any): Product[] {
        const sitemapProducts: ISitemapProduct[] = this.parseXML(res.body)
        // tslint:disable-next-line: max-line-length
        // the first product in sitemap response the home page of the site and not an actual product. Slice the array to remove the first product
        return sitemapProducts.map(this.parseProduct)
    }

    public parseXML(body: string): ISitemapProduct[] {
        return xmlParser.parse(body, {
            ignoreAttributes: false,
            attributeNamePrefix: ''
        }).urlset.url
    }

    public parseProduct(sitemapProduct: ISitemapProduct): Product {
        const { loc } = sitemapProduct
        const titleMatches: string[] | null = loc.match(/(?<=\/products\/).*$/g)
        const title = titleMatches ? titleMatches[0] : 'Title not found'
        return new Product({
            title,
            link: loc,
            id: title
        })
    }
}

// only run the monitor if this file is not being imported
if (module.parent === null) {
    startMonitor(
        SitemapMonitor, process.argv.slice(2))
}
