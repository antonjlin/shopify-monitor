import cheerio from 'cheerio'
import randomInt from 'random-int'
import rp from 'request-promise'

import { Product } from '../models/product'
import { Site } from '../models/site'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class DSMMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[]) {
        super(site, `http://${site.domain}`, proxies)
        this.scrapeRestockLink = true
    }

    public getOptions() {
        return super.getOptions({
            qs: {
                page: randomInt(10000000, 100000000)
            },
            json: false
        })
    }

    public async getResponse(options: rp.Options) {
        return rpt(options)
    }

    public parseResponse(res: any): Product[] {
        const $ = cheerio.load(res.body)
        const products = $('.grid-view-item__link')
            .map((_: any, link: any) => {
                return this.parseProduct($, link)
            })
            .get()
        return products
    }

    public parseProduct($: CheerioStatic, link: any): Product {
        const title = $(link)
            .children('.h4.grid-view-item__title')
            .text()
            .trim()
        return new Product({
            title,
            link: 'https://' + this.site.domain + $(link).attr('href'),
            id: title
        })
    }
}

startMonitor(DSMMonitor, process.argv.slice(2))
