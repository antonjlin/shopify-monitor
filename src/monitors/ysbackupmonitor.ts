import { Site } from '../models/site'
import { startMonitor } from '../utils/startMonitor'
import { HTMLMonitor } from './htmlmonitor'

export class YSBackupMonitor extends HTMLMonitor {
    constructor(site: Site, proxies: string[]) {
        super(site, proxies)
        this.endpoint = `https://${site.domain}`

    }
}

startMonitor(YSBackupMonitor, process.argv.slice(2))
