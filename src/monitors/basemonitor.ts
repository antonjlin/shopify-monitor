import rp from 'request-promise'
import uuid from 'uuid'

import { Discount, Product } from '../models/product'
import { ProductListings } from '../models/productlistings'
import { Site } from '../models/site'
import { MonitorSource } from '../types/basetypes'
import { Timer } from '../utils/timer'
import { BaseScraper } from './basescraper'
import md5 from 'md5'
import fs from 'fs'
import path from 'path'

export class BaseMonitor extends BaseScraper {
    public source: MonitorSource

    public prevScrapeSizes: number[] = []
    public mostRecentScrape = 0
    public isFirstScrape = true
    public isLinkBroken = false
    public isPasswordUp = false
    public currentProducts = new ProductListings()
    public responseTimes: number[] = []
    public previousHash: string = ''

    constructor(public site: Site, public endpoint: string, proxies: string[]) {
        super(proxies, 750, 5000)
        this.source = {
            endpoint: this.endpoint,
            delay: this.delay,
            timeout: this.timeout,
            site: this.site,
            monitor: this.constructor.name
        }
        this.logger.addTag(this.site.domain)
    }

    // DEPRECATED
    public async getProducts(): Promise<Product[]> {
        throw new Error('Method not implemented.')
    }

    // tslint:disable-next-line: ban-types
    public getOptions(overrideParams: Object = {}): rp.Options {
        return super.getOptions(this.endpoint, overrideParams)
    }

    public async getResponse(options: rp.Options): Promise<any> {
        throw new Error('Method not implemented.')
    }

    public parseResponse(_: any): Product[] {
        throw new Error('Method not implemented.')
    }

    public dataToString(body: any): string {
        if (typeof body === 'string') { return body }
        if (typeof body === 'object') { return JSON.stringify(body) }

        // Shouldn't happen because type should always be string or object.
        // So just return random string
        return uuid()
    }

    public async run() {
        setTimeout(() => {
            this.run()
        }, this.delay)
        const timer = new Timer()
        try {
            if (!this.stopped) {
                const options = this.getOptions()

                const time = Date.now()
                this.mostRecentScrape = time
                const res = await this.getResponse(options)
                this.datadogSender.logRequestSuccess(
                    res.statusCode,
                    res.elapsedTime,
                    this.source
                )

                // fs.writeFileSync(path.resolve(__dirname, `${time}.txt`), res.body)

                this.updatePasswordPage(
                    res.request &&
                    res.request.href &&
                    res.request.href.includes('/password')
                )

                const products = this.isPasswordUp
                    ? []
                    : await this.parseResponse(res)

                this.logger.info(products.length)
                // console.log(products.length)

                const responseTime = this.timer.elapsed()
                this.adjustTimeout(responseTime)

                const discountedProducts: Discount[] = this.currentProducts.getDiscountedProducts(
                    products
                )
                let restockedProducts: Product[] = this.currentProducts.getRestockedProducts(
                    products
                )

                // ignore products if:
                // there were more than 100 products restocked
                // all restocked products were newly "added" products instead of existing products
                // the current scrape size is equal to the scrape size 5 scrapes ago (+/- 10 products)
                const curScrapeSize = this.currentProducts.products.length
                if (
                    restockedProducts.length > 100 &&
                    curScrapeSize - restockedProducts.length ===
                    this.prevScrapeSizes[0] &&
                    (curScrapeSize > this.prevScrapeSizes[4] - 10 &&
                        curScrapeSize < this.prevScrapeSizes[4] + 10)
                ) {
                    restockedProducts = []
                    this.datadogSender.ignoreRestockEvent(
                        [curScrapeSize, ...this.prevScrapeSizes],
                        restockedProducts.length,
                        this.source
                    )
                }
                this.prevScrapeSizes.splice(0, 0, curScrapeSize) // insert at beginning of array
                this.prevScrapeSizes.length > 5 &&
                    this.prevScrapeSizes.pop()

                if (!this.isFirstScrape) {
                    this.sendNotifications(restockedProducts, this.source)
                    this.sendDiscountNotifications(discountedProducts)
                } else {
                    this.isFirstScrape = false
                    if (this.testConfig.sendInitialScrape) {
                        this.sendNotifications(
                            restockedProducts,
                            this.source
                        )
                    }
                }

                /*if (time >= this.mostRecentScrape) {
                    this.updatePasswordPage(
                        res.request &&
                        res.request.href &&
                        res.request.href.includes('/password')
                    )
                    console.log(typeof res)
                    const products = this.isPasswordUp
                        ? []
                        : await this.parseResponse(res)

                    const responseTime = this.timer.elapsed()
                    this.adjustTimeout(responseTime)

                    const discountedProducts: Discount[] = this.currentProducts.getDiscountedProducts(
                        products
                    )
                    let restockedProducts: Product[] = this.currentProducts.getRestockedProducts(
                        products
                    )

                    // ignore products if:
                    // there were more than 100 products restocked
                    // all restocked products were newly "added" products instead of existing products
                    // the current scrape size is equal to the scrape size 5 scrapes ago (+/- 10 products)
                    const curScrapeSize = this.currentProducts.products.length
                    if (
                        restockedProducts.length > 100 &&
                        curScrapeSize - restockedProducts.length ===
                        this.prevScrapeSizes[0] &&
                        (curScrapeSize > this.prevScrapeSizes[4] - 10 &&
                            curScrapeSize < this.prevScrapeSizes[4] + 10)
                    ) {
                        restockedProducts = []
                        this.datadogSender.ignoreRestockEvent(
                            [curScrapeSize, ...this.prevScrapeSizes],
                            restockedProducts.length,
                            this.source
                        )
                    }
                    this.prevScrapeSizes.splice(0, 0, curScrapeSize) // insert at beginning of array
                    this.prevScrapeSizes.length > 5 &&
                        this.prevScrapeSizes.pop()

                    if (!this.isFirstScrape) {
                        this.sendNotifications(restockedProducts, this.source)
                        this.sendDiscountNotifications(discountedProducts)
                    } else {
                        this.isFirstScrape = false
                        if (this.testConfig.sendInitialScrape) {
                            this.sendNotifications(
                                restockedProducts,
                                this.source
                            )
                        }
                    }
                } */
            }
        } catch (err) {
            const responseTime = timer.elapsed()
            this.adjustTimeout(responseTime)
            this.catchError(err)
        }
        this.datadogSender.logProductListingsSize(
            this.currentProducts.products.length,
            this.source
        )
    }

    public catchError(err: any): void {
        const { statusCode } = err
        if (err.message && (
            err.message.includes('MANUAL_TIMEOUT') ||
            err.message.includes('TIMEDOUT') ||
            err.message.includes('tunneling socket could not be established') ||
            err.message.includes('socket hang up')
        )) {
            this.datadogSender.logRequestTimeout(
                statusCode,
                this.timeout,
                this.source
            )
        } else if (statusCode) {
            switch (statusCode) {
                case 401:
                    this.datadogSender.logRequestPassword(
                        statusCode,
                        this.source
                    )
                    this.updatePasswordPage(true)
                    break
                case 429:
                case 430:
                    this.datadogSender.logRequestBan(
                        statusCode,
                        err.code,
                        this.source
                    )
                    break
                default:
                    this.datadogSender.logRequestError(
                        statusCode,
                        err.code,
                        this.source
                    )
                    break
            }
        } else if (err.message.includes('incorrect header check')) {
            this.datadogSender.logRequestError(999, err.code, this.source)
        } else if (
            err.message.includes(
                'Client network socket disconnected before secure TLS connection'
            )
        ) {
            this.datadogSender.logRequestError(111, err.code, this.source)
        } else {
            this.datadogSender.logRequestError(
                statusCode,
                err.code,
                this.source
            )
            this.logger.error(err)
        }
    }

    public adjustTimeout(responseTime: number) {
        this.responseTimes.push(responseTime)
        if (this.responseTimes.length > 50) {
            this.responseTimes.shift()
        }
        const ninetyFifthPercentile = this.responseTimes
            .slice()
            .sort((a, b) => a - b)[Math.floor(this.responseTimes.length * 0.95)]
        this.timeout = Math.min(ninetyFifthPercentile * 2, 60000)
        this.datadogSender.logDynamicTimeout(this.timeout, this.source)
    }

    public updatePasswordPage(isPasswordUp = false): void {
        if (
            this.constructor.name !== 'LinkThread' &&
            this.constructor.name !== 'DirectLinkMonitor' &&
            isPasswordUp !== this.isPasswordUp
        ) {
            this.isPasswordUp = isPasswordUp
            if (isPasswordUp) {
                this.logger.error(`Password page up for ${this.site.domain}`)
                this.sendPasswordNotification(true, !this.isFirstScrape)
            } else {
                this.sendPasswordNotification(false, !this.isFirstScrape)
                this.logger.info(`Password page down for ${this.site.domain}`)
            }
            this.currentProducts.clear()
            this.isFirstScrape = false
        }
    }

    public async registerIPCClient(): Promise<void | {}> {
        return new Promise((resolve, _) => {
            this.ipc.registerListener('connect', () => {
                this.ipcConnected = true
                this.ipc.emit('monitor.connected', this.constructor.name)
            })
            resolve()
        })
    }

    public sendPasswordNotification(isPasswordUp: boolean, sendNotification: boolean) {
        super.sendPasswordNotification(
            isPasswordUp,
            sendNotification,
            this.source
        )
    }

    public sendDiscountNotifications(discounts: Discount[]) {
        super.sendDiscountNotifications(discounts, this.source)
    }

    // Given a data string, clear it of whitespace and extreneous things like \n,
    // then return the hashed string through md5
    public hashData(data: string): string {
        return md5(data)
    }

    // Given new webdata (in string form) turn it into a hash then compare it to the previous hash.
    // Set new hash if the hashes are different.
    public isWebsiteDifferent(webData: string): boolean {
        return true
        const newHash = this.hashData(webData)
        if (!this.previousHash) {
            this.previousHash = newHash
            return true
        }

        const equal = newHash === this.previousHash

        if (!equal) {
            this.previousHash = newHash
        }

        // I flipped the value of equal because this function returns if the website is different
        // it should return true if the new hash is different. So we have to inverse the value of equal
        // to reflect the purpose of the function
        return !equal
    }
}
