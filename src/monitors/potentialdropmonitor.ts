import rp from 'request-promise'

import { Site } from '../models/site'
import { MonitorSource } from '../types/basetypes'
import { DatadogSender } from '../utils/datadogsender'
import { IPCClient } from '../utils/ipcclient'
import { defaultLogger } from '../utils/logger'
import { ProxyManager } from '../utils/proxymanager'
import { UserAgentGenerator } from '../utils/useragentgenerator'

interface IRunningInstance {
    source: MonitorSource
    currentProductNumber: number
    sendFiltered: boolean
}

export class PotentialDropMonitor {
    public runningInstances: IRunningInstance[] = []

    public delay: number = 15000 // Change ??
    public timeout: number = 5000

    public userAgentGenerator = new UserAgentGenerator()
    public datadogSender = new DatadogSender()
    public proxyManager = new ProxyManager(['localhost'])
    public logger = defaultLogger(this.constructor.name, false, './logs')
    public ipc = new IPCClient('server')

    constructor() {
        this.run()
    }

    public async registerIPCClient(): Promise<void | {}> {
        return new Promise((resolve, _) => {
            this.ipc
                .registerListener('connect', () => {
                    this.ipc.emit('potential.monitor.connected')
                    this.logger.info('Potential Monitor Connected.')
                })
                .registerListener('proxy', (data: any) => {
                    this.proxyManager = new ProxyManager(data)
                })
                .registerListener('potential.add', (site: any) => {
                    this.logger.info(
                        `Starting Potential Monitor for ${site.domain}`
                    )
                    this.addMonitor(site)
                })
                .registerListener('potential.remove', (site: any) => {
                    this.logger.info(
                        `Stopping Potential Monitor for ${site.domain}`
                    )
                    this.removeMonitor(site)
                })
            resolve()
        })
    }

    public async start() {
        await this.registerIPCClient()
        this.run()
    }

    public async run() {
        this.runningInstances.forEach((instance: IRunningInstance) => {
            this.compareInfo(instance).catch((err) => {
                this.catchError(err, instance)
            })
        })
        setTimeout(() => {
            this.run()
        }, this.delay)
    }

    public catchError(err: any, instance: IRunningInstance): void {
        const { statusCode } = err
        const errorMessage =
            err instanceof Error ? err.stack : JSON.stringify(err)
        if (statusCode) {
            switch (statusCode) {
                case 401:
                    break
                case 429:
                case 430:
                    this.datadogSender.logRequestBan(
                        statusCode,
                        err.code,
                        instance.source
                    )
                    break
                default:
                    this.datadogSender.logRequestError(
                        statusCode,
                        err.code,
                        instance.source
                    )
                    break
            }
        } else if (
            err.message && (
                err.message.includes('TIMEDOUT') ||
                err.message.includes('tunneling socket could not be established') ||
                err.message.includes('socket hang up'))
        ) {
            this.datadogSender.logRequestTimeout(
                statusCode,
                this.timeout,
                instance.source
            )
            // this.logger.timeout(err)
        } else if (err.message.includes('incorrect header check')) {
            // do nothing. this is an error thrown by the requests module when gzip fails
            // unexpectedely and we can't do anything about that.
        } else {
            this.logger.error(err)
        }
    }

    public async getProductNum(instance: IRunningInstance): Promise<number> {
        const options = {
            url: instance.source.endpoint,
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'User-Agent': this.userAgentGenerator.get()
            },
            timeout: this.timeout,
            time: true,
            gzip: true,
            json: true,
            rejectUnauthorized: false,
            resolveWithFullResponse: true,
            proxy: this.proxyManager.get()
        }
        try {
            const res = await rp(options)
            this.datadogSender.logRequestSuccess(
                res.statusCode,
                res.elapsedTime,
                instance.source
            )
            return res.body.published_products_count
        } catch (err) {
            throw err
        }
    }

    public async compareInfo(instance: IRunningInstance) {
        try {
            if (instance.currentProductNumber === -1) {
                instance.currentProductNumber = await this.getProductNum(
                    instance
                )
            } else {
                const newProductNum: number = await this.getProductNum(instance)
                const prodNumDifference: number =
                    newProductNum - instance.currentProductNumber
                if (prodNumDifference > 0) {
                    instance.currentProductNumber = newProductNum
                    this.sendPotentialDropNotification(
                        prodNumDifference,
                        instance
                    )
                    instance.sendFiltered = false
                }
            }
        } catch (err) {
            throw err
        }
    }

    public sendPotentialDropNotification(
        productNumDifference: number,
        instance: IRunningInstance
    ) {
        this.ipc.emit('potential.update', {
            productNumDifference,
            source: instance.source,
            sendFiltered: instance.sendFiltered
        })
    }

    public findSite(site: Site): IRunningInstance | undefined {
        const foundSites = this.runningInstances.filter(
            (instance: IRunningInstance) => {
                return instance.source.site === site
            }
        )
        return foundSites.length > 0 ? foundSites[0] : undefined
    }

    public addMonitor(site: Site) {
        if (this.findSite(site) === undefined) {
            const source = {
                endpoint: `${site.url}/meta.json`,
                delay: this.delay,
                timeout: this.timeout,
                site,
                monitor: this.constructor.name
            }
            this.runningInstances.push({
                source,
                currentProductNumber: -1,
                sendFiltered: true
            })
        }
    }

    public removeMonitor(site: Site) {
        const removeInstance = this.findSite(site)
        if (removeInstance !== undefined) {
            this.runningInstances.splice(
                this.runningInstances.indexOf(removeInstance)
            )
            this.logger.success('Potential Monitor instance killed.')
        }
    }
}

const potentialDropMon = new PotentialDropMonitor()
potentialDropMon.start()

// const site: Site = {
//     displayName: 'calicosiotest',
//     domain: 'calicosiotest.myshopify.com',
//     url: 'https://calicosiotest.myshopify.com',
//     currency: 'USD',
//     groups: ['Calicos']
// }
// potentialDropMon.addMonitor(site)

// potentialDropMon.removeMonitor(site)
