import * as randomstring from 'randomstring'
import rp from 'request-promise'

import { Product, Variant } from '../models/product'
import { Site } from '../models/site'
import {
    IProductJsonProduct,
    IProductJSONResponse,
} from '../types/productjsontypes'
import { rpt } from '../utils/requestpromisetimeout'
import { startMonitor } from '../utils/startMonitor'
import { BaseMonitor } from './basemonitor'

export class ProductJSONMonitor extends BaseMonitor {
    constructor(site: Site, proxies: string[]) {
        super(site, `https://${site.domain}/products.json`, proxies)
    }

    public getOptions() {
        const callbackQs: string = randomstring.generate({
            length: 45,
            charset: 'alphabetic'
        })
        const jsonPattern = new RegExp(`${callbackQs}\\((.*)\\)$`, 'g')
        return super.getOptions({
            qs: {
                limit: 50,
                callback: callbackQs
            },
            transform: (body: any, response: any) => {
                const matches = jsonPattern.exec(body)
                if (matches !== null) { response.body = JSON.parse(matches[1]) }
                return response
            }
        })
    }

    public async getResponse(options: rp.Options) {
        return rpt(options)
    }

    public parseResponse(res: IProductJSONResponse): Product[] {
        return res.body.products.map(this.parseProduct.bind(this))
    }

    public parseProduct(product: IProductJsonProduct): Product {
        const { title, handle, id, variants, images, tags } = product
        const price = variants.length > 0 ? variants[0].price : ''
        return new Product({
            title,
            tags: tags || [],
            link: `https://${this.site.domain}/products/${handle}`,
            id: id.toString(),
            price: Product.normalizePrice(Number(price), this.site.currency),
            variants: variants.map((x: any) => this.parseVariants(x)),
            image: images && images.length > 0 ? images[0].src : null
        })
    }

    public parseVariants(variant: any): Variant {
        const { id, title, available } = variant
        return {
            title,
            id,
            available,
            atcLink: Product.getATCLink(this.site.url, id)
        }
    }
}

startMonitor(ProductJSONMonitor, process.argv.slice(2))
