import rp from 'request-promise'
import {
    DashboardGroup,
    DashboardKeywords,
    DashboardProxiesResponse,
    DashboardSite,
    DashboardWebhook,
} from '../types/dashboardtypes'

export const bopifyDashboard = {
    async getSites(): Promise<DashboardSite[]> {
        const options = {
            url: 'https://bopify-dashboard.herokuapp.com/api/sites',
            json: true
        }
        return rp(options).then((res: DashboardSite[]) => {
            return res
        })
    },
    async getProxies(): Promise<string[]> {
        const options = {
            url: 'https://bopify-dashboard.herokuapp.com/api/proxies',
            json: true
        }
        return rp(options).then((res: DashboardProxiesResponse) => {
            return res.proxies
        })
    },
    async getKeywords(): Promise<DashboardKeywords[]> {
        const options = {
            url: 'https://bopify-dashboard.herokuapp.com/api/keywords',
            json: true
        }
        return rp(options).then((res: DashboardKeywords[]) => {
            return res
        })
    },
    async getWebhooks(): Promise<DashboardWebhook[]> {
        const options = {
            url: 'https://bopify-dashboard.herokuapp.com/api/webhooks',
            json: true
        }
        return rp(options)
    },
    async getGroups(): Promise<DashboardGroup[]> {
        const options = {
            url: 'https://bopify-dashboard.herokuapp.com/api/groups',
            json: true
        }
        return rp(options)
    }
}
