import * as cp from 'child_process'
import shell from 'shelljs'
import uuid from 'uuid'

import { defaultLogger } from './logger'

export class ChildProcesses {
    public logger = defaultLogger('Child Process Manager', this.silent)
    public children: { [key: string]: cp.ChildProcess } = {}
    public numDisconnected = 0

    constructor(public silent: boolean = false) { }

    public launch(
        path: string,
        args: any[],
        timeoutMilliseconds?: number
    ) {
        this.logger.init(`Spawning child from: ${path}`)
        setImmediate(() => {
            const child = cp.fork(path, args)
            const index = uuid()
            this.children[index] = child
            if (typeof timeoutMilliseconds === 'number') {
                setTimeout(() => {
                    child.kill()
                    this.logger.boost('Killed child process')
                }, timeoutMilliseconds)
            }
            child.on('close', (code) => {
                this.numDisconnected += 1
                this.logger.error(
                    `Child process with path ${path} and args ${args.slice(
                        0,
                        2
                    )} exited with exit code ${code}`
                )
                delete this.children[index]
            })
        })
    }

    public killAll() {
        Object.keys(this.children).forEach((childIndex) => {
            this.children[childIndex].kill('SIGINT')
        })
        this.children = {}
        shell.exec('pkill -f /monitors/')
    }

    get length() {
        return Object.keys(this.children).length
    }
}
