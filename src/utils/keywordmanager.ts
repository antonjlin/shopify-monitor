import { Product } from '../models/product'
import { defaultLogger } from './logger'

export class KeywordManager {
    logger = defaultLogger('KeywordManager')

    constructor(public keywordSets: string[] = []) {}

    public filter(textOrProduct: string): string | undefined
    public filter(textOrProduct: Product): string | undefined
    public filter(textOrProduct: any): string | undefined {
        if (typeof textOrProduct === 'string') {
            for (const keywordSet of this.keywordSets) {
                if (keywordSet) {
                    if (this.matchesKeywordSet(textOrProduct, keywordSet)) {
                        return keywordSet
                    }
                }
            }
        } else if (textOrProduct.title) {
            const text = this.constructKeywordCheckString(textOrProduct)
            return this.filter(text)
        }
        return undefined
    }

    public matchesKeywordSet(text: string, keywordSet: string) {
        const keywords: string[] = keywordSet
            .trim()
            .split(',')
            .filter((str) => str !== '')
        text = text.toLowerCase()
        let matches = true
        keywords.forEach((keyword) => {
            const sign = keyword[0]
            if (sign !== '+' && sign !== '-') {
                const errorMessage = `${keywordSet} contains sign other than +/-: ${sign}`
                this.logger.error(errorMessage)
            } else {
                const word = keyword.substring(1).toLowerCase()
                if (word.includes('|')) {
                    return this.matchesOrKeyword(sign, word, text)
                } else {
                    if (sign === '+' && !text.includes(word)) {
                        matches = false
                    } else if (sign === '-' && text.includes(word)) {
                        matches = false
                    }
                }
            }
        })
        return matches
    }

    public matchesOrKeyword(sign: string, keyword: string, text: string) {
        const words = keyword.split('|')
        words.forEach((word) => {
            if (sign === '+' && text.includes(word)) { return true }
            if (sign === '-' && text.includes(word)) { return false }
        })
        return sign === '-'
    }

    public constructKeywordCheckString(product: Product) {
        const { title, variants, tags, image } = product
        let string = title + product.link
        if (variants) {
            string += variants.map((variant) => variant.title).join(' ')
        }
        if (tags) {
            string += tags.toString()
        }
        if (image) {
            string += image
        }
        return string
    }
}
