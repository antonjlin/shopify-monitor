export class Timer {
    public prevTime = Date.now()
    constructor() {
        // todo
    }

    public reset() {
        this.prevTime = Date.now()
    }

    public elapsed() {
        const elapsed = Date.now() - this.prevTime
        this.prevTime = Date.now()
        return elapsed
    }
}
