import rp from 'request-promise'

export const rpt = (options: rp.Options): Promise<any> => {
    return new Promise((resolve, reject) => {
        rp(options)
            .then(resolve)
            .catch(reject)
        setTimeout(() => {
            reject({ message: 'MANUAL_TIMEOUT', statusCode: 69 })
        }, options.timeout || 5000)
    })
}
