import moment from 'moment-timezone'
import rp from 'request-promise'
import { BoostConfig } from '../types/boosttypes'
import { defaultLogger } from './logger'

const discordHook =
    'https://discordapp.com/api/webhooks/503084475186675712/E4ZqcP2xnq4qabOP834YCWqGV9KyvLr0pZqbdUqOuKBEfNGOrfN1lhRd9K1czK51NpSy/slack'

const logger = defaultLogger('Discord Github')

export const postGitUpdate = (changelogString: string) => {
    logger.info('Sending postgitupdate')
    rp({
        method: 'POST',
        url: discordHook,
        body: {
            text: '',
            attachments: [
                {
                    fallback: 'bopify',
                    color: '#00ff00',
                    title: `Vital Orb Deployed - Server #${process.env
                        .SERVER_ID || 'N/A'}`,
                    text: 'Changelog:\n' + changelogString,
                    footer: moment()
                        .tz('America/Los_Angeles')
                        .format('hh:mm:ss A zz')
                }
            ]
        },
        json: true
    })
        .then((res) => {
            logger.info(res)
        })
        .catch((err) => {
            logger.error(err)
        })
}

export const sendBoost = (boostConfig: BoostConfig) => {
    const { domain, threadMultiplier, duration } = boostConfig
    logger.info('Sending BOOST update')
    rp({
        method: 'POST',
        url: discordHook,
        body: {
            text: '',
            attachments: [
                {
                    fallback: 'bopify',
                    color: '#00ff00',
                    title: `Vital Orb - BOOST ACTIVATED on ${domain} - ${threadMultiplier} threads - ${duration} minutes`,
                    text: '',
                    footer: moment()
                        .tz('America/Los_Angeles')
                        .format('hh:mm:ss A zz')
                }
            ]
        },
        json: true
    })
        .then((res) => {
            logger.info(res)
        })
        .catch((err) => {
            logger.error(err)
        })
}

export const allMonitorsConnected = (numMonitors: number) => {
    logger.info('Sending monitors connected update')
    rp({
        method: 'POST',
        url: discordHook,
        body: {
            text: '',
            attachments: [
                {
                    fallback: 'bopify',
                    color: '#00ff00',
                    title: `Vital Orb - All ${numMonitors} monitors connected`,
                    text: '',
                    footer: moment()
                        .tz('America/Los_Angeles')
                        .format('hh:mm:ss A zz')
                }
            ]
        },
        json: true
    })
        .then((res) => {
            logger.info(res)
        })
        .catch((err) => {
            logger.error(err)
        })
}
