import { restraints } from '../data/grouprestraints'
import { restockIDIgnore } from '../data/restockidignore'
import { DiscountEmbed } from '../notifications/discountembed'
import { groupFormats } from '../notifications/groupformats'
import {
    NotificationHookManager
} from '../notifications/notificationhookmanager'
import { PasswordEmbed } from '../notifications/passwordembed'
import { PotentialDropEmbed } from '../notifications/potentialdropembed'
import { ProductEmbed } from '../notifications/productembed'
import { SubDomainEmbed } from '../notifications/subdomainembed'
import { Group } from '../types/basetypes'
import {
    DashboardGroup,
    DashboardGroupName,
    DashboardKeywords,
    DashboardSite,
    DashboardWebhook
} from '../types/dashboardtypes'
import {
    DiscountNotification,
    PasswordNotification,
    PotentialDropNotification,
    ProductNotification,
    SubDomainNotification,
    Webhook
} from '../types/notificationtypes'
import { DatadogSender } from './datadogsender'
import { IPCClient } from './ipcclient'
import { KeywordManager } from './keywordmanager'
import { defaultLogger } from './logger'
import { RestockIDManager } from './restockidmanager'
import { RestockWebsocket } from './restockwebsocket'

export class NotificationManager {
    public restockIDManager = new RestockIDManager(15000)
    public logger = defaultLogger('Notification Manager')
    public datadog = new DatadogSender()
    public ws = new RestockWebsocket(8081)
    public ipc = new IPCClient('server')

    public groups: { [key: string]: Group }

    constructor(
        public dashboardGroups: DashboardGroup[] = [],
        public sites: DashboardSite[] = [],
        public keywords: DashboardKeywords[] = [],
        public webhooks: DashboardWebhook[] = []
    ) {
        this.groups = this.syncGroups(dashboardGroups, keywords, webhooks)
    }

    public syncGroups(
        dashboardGroups: DashboardGroup[],
        keywords: DashboardKeywords[],
        webhooks: DashboardWebhook[]
    ): { [key: string]: Group } {
        return dashboardGroups
            .map((dashboardGroup) => {
                const groupName = dashboardGroup.group
                try {
                    const group = {
                        name: groupName,
                        keywordManager: new KeywordManager(
                            this.getGroupKeywords(groupName, keywords)
                        ),
                        sender: new NotificationHookManager(
                            this.getGroupWebhook(groupName, webhooks)
                        ),
                        format: groupFormats[groupName]
                    }
                    return group
                } catch (err) {
                    this.logger.error(`Invalid group name ${groupName}`)
                    throw new Error(`Invalid group name ${groupName}`)
                }
            })
            .filter((group) => group.format)
            .reduce((acc, cur) => {
                return Object.assign(acc, { [cur.name]: cur })
            }, {})
    }

    public syncKeywordsAndWebhooks(
        keywords: DashboardKeywords[],
        webhooks: DashboardWebhook[]
    ) {
        this.keywords = keywords
        this.webhooks = webhooks
        this.groups = this.syncGroups(
            this.dashboardGroups,
            this.keywords,
            this.webhooks
        )
    }

    public sendRestockNotification(productNotification: ProductNotification) {
        const { product, source } = productNotification
        let isAvailable: boolean
        if (product.variants) {
            isAvailable =
                product.variants.filter((variant) => variant.available).length > 0
        } else {
            isAvailable = false
        }
        if (
            this.restockIDManager.isValidRestock(product) ||
            restockIDIgnore.includes(source.monitor)
        ) {
            this.logger.restock(`${source.monitor}: ${product.title}`)
            if (
                source.monitor === 'DSMMonitor' ||
                source.monitor === 'YeezySupplyMonitor' ||
                source.monitor === 'HTMLMonitor' ||
                source.monitor === 'MondayProgramMonitor' ||
                source.monitor === 'YSBackupMonitor' ||
                isAvailable
            ) {
                source.site.groups.forEach((groupName) => {
                    const group = this.groups[groupName]
                    if (group) {
                        const keywordSet = group.keywordManager.filter(product)
                        const filtered = Boolean(keywordSet)
                        productNotification.keywordSet = keywordSet
                        group.sender.send(
                            new ProductEmbed(productNotification, group.format),
                            filtered ? 'filtered' : 'unfiltered',
                            [group.name]
                        )
                    } else {
                        this.logger.error(`Invalid group name ${groupName} `)
                    }
                })
            }
            this.datadog.restockEvent(productNotification, isAvailable)
            if (product.variants && isAvailable) {
                const { url, extraMeta } = source.site
                this.ws.broadcast(Object.assign(product, { monitor: 'shopify', site: { url, extraMeta } }))
            }
        }
    }

    public sendPasswordNotification(passwordNotification: PasswordNotification) {
        const { source, isPasswordUp, sendNotification } = passwordNotification

        if (
            this.restockIDManager.isValidPassword(
                source.site.domain,
                isPasswordUp
            )
        ) {
            this.logger.password(
                `${source.site.displayName} password page is ${isPasswordUp} `
            )
            sendNotification &&
                this.sendAllNotifications(
                    source.site.groups,
                    passwordNotification,
                    PasswordEmbed,
                    'filtered'
                )
            if (isPasswordUp) {
                this.ipc.emit('potential.add', {
                    source
                })
            } else {
                this.ipc.emit('potential.remove', {
                    source
                })
            }
        }
    }

    public sendPotentialDropNotification(
        potentialDropNotification: PotentialDropNotification
    ) {
        this.sendAllNotifications(
            restraints.potentialdrop,
            potentialDropNotification,
            PotentialDropEmbed,
            potentialDropNotification.sendFiltered ? 'filtered' : 'unfiltered'
        )
    }

    public sendDiscountNotification(discountNotification: DiscountNotification) {
        const { discountInfo, source } = discountNotification

        if (this.restockIDManager.isValidDiscount(discountInfo.product)) {
            this.logger.discount(
                `${source.monitor}: ${discountInfo.product.title} `
            )
            this.sendAllNotifications(
                source.site.groups,
                discountNotification,
                DiscountEmbed,
                'discount'
            )
        }
    }

    public sendSubDomainNotification(subdomainNotification: SubDomainNotification) {
        this.sendAllNotifications(
            restraints.subdomain,
            subdomainNotification,
            SubDomainEmbed,
            'filtered'
        )
    }

    public sendAllNotifications(
        groupNames: string[],
        notification: any,
        embed:
            | typeof ProductEmbed
            | typeof PasswordEmbed
            | typeof DiscountEmbed
            | typeof PotentialDropEmbed
            | typeof SubDomainEmbed,
        channel: 'unfiltered' | 'filtered' | 'discount'
    ) {
        groupNames.forEach((groupName) =>
            this.sendNotification(groupName, notification, embed, channel)
        )
    }

    public sendNotification(
        groupName: string,
        notification: any,
        embed:
            | typeof ProductEmbed
            | typeof PasswordEmbed
            | typeof DiscountEmbed
            | typeof PotentialDropEmbed
            | typeof SubDomainEmbed,
        channel: 'unfiltered' | 'filtered' | 'discount'
    ) {
        const group = this.groups[groupName]
        if (group) {
            group.sender.send(new embed(notification, group.format), channel)
        } else {
            this.logger.error(`Invalid group name ${groupName} `)
        }
    }

    public getGroupKeywords(
        groupName: DashboardGroupName,
        keywords: DashboardKeywords[]
    ): string[] {
        const matches = keywords.filter((keyword) => keyword.group === groupName)
        if (matches.length > 0) {
            return matches[0].keywords
        }
        this.logger.error(`No keywords found for ${groupName}`)
        return []
    }

    public getGroupWebhook(groupName: string, webhooks: DashboardWebhook[]): Webhook {
        const matches = webhooks
            .filter((webhook) => webhook.group === groupName)
            .map((webhook) => {
                const { filtered, unfiltered, discounts } = webhook
                return {
                    filtered,
                    unfiltered,
                    discounts
                }
            })
        if (matches.length > 0) {
            return matches[0]
        }
        this.logger.error(`No webhooks found for ${groupName}`)
        return { filtered: [], unfiltered: [], discounts: [] }
    }
}
