import { Product } from '../models/product'

export class RestockIDManager {
    public restockIDs: { [restockID: string]: null } = {}

    constructor(public delay: number) {}

    public isValidRestock(product: Product): boolean {
        const restockID = this.buildRestockId(product)
        if (restockID === null) { return true }
        return this.isValidRestockID('Restock:' + restockID)
    }

    public isValidPassword(siteUrl: string, isPasswordUp: boolean): boolean {
        const restockID = `Site:${siteUrl}|PasswordUp:${isPasswordUp}`
        return this.isValidRestockID(restockID)
    }

    public isValidDiscount(product: Product): boolean {
        const restockID = this.buildRestockId(product)
        if (restockID === null) { return true }
        return this.isValidRestockID('Discount:' + restockID)
    }

    public isValidRestockID(restockID: string): boolean {
        const isValid = !Object.keys(this.restockIDs).includes(restockID)
        if (isValid) {
            this.restockIDs[restockID] = null
            setTimeout(() => delete this.restockIDs[restockID], this.delay)
        }
        return isValid
    }

    public buildRestockId(product: Product): string | null {
        if (product.variants) {
            let restockID = `${product.id}|${product.title}|${product.link}`
            product.variants.forEach((variant) => {
                if (variant.available) { restockID += variant.id }
            })
            return restockID
        }
        return null
    }
}
