import UserAgent from 'user-agents'

export class UserAgentGenerator {
    public get(): string {
        // REDACTED
        return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36'
    }
}
