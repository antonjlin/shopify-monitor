import * as ipc from 'node-ipc'
import { defaultLogger, Logger } from './logger'

ipc.config.silent = true
ipc.config.appspace = 'bopify.'

export class IPCClient {
    public server: any
    public logger: Logger = defaultLogger('IPC Client')

    constructor(public serverName: string = 'server') {
        this.server = this.connect()
    }

    public connect(): any {
        ipc.connectTo(this.serverName)
        return ipc.of[this.serverName]
    }

    public emit(...args: any): IPCClient {
        this.server.emit(...args)
        return this
    }

    public registerListener(
        event: string,
        callback: (data: object) => void
    ): IPCClient {
        this.server.on(event, callback)
        return this
    }
}
