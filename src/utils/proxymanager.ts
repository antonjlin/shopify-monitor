const proxylessMode = false // (process.env.PROXYLESS || 'false').toLowerCase() === 'true'
// TODO: change this once we get closer to production. not sure about env

export class ProxyManager {
    public proxies: Array<string | undefined>

    constructor(proxies: string[]) {
        this.proxies = proxies.map(this.formatProxy)
        this.shuffle(this.proxies)
    }

    public shuffle(arr: Array<string | undefined>): Array<string | undefined> {
        let temp1
        let temp2
        for (let i = arr.length - 1; i > 0; i--) {
            temp1 = Math.floor(Math.random() * (i + 1))
            temp2 = arr[i]
            arr[i] = arr[temp1]
            arr[temp1] = temp2
        }
        return arr
    }

    public formatProxy(proxy: string): string | undefined {
        if (proxy && ['localhost', ''].indexOf(proxy) < 0) {
            proxy = proxy.replace(' ', '_')
            const proxySplit = proxy.split(':')
            if (proxySplit.length > 3) {
                return (
                    'http://' +
                    proxySplit[2] +
                    ':' +
                    proxySplit[3] +
                    '@' +
                    proxySplit[0] +
                    ':' +
                    proxySplit[1]
                )
            } else {
                return 'http://' + proxySplit[0] + ':' + proxySplit[1]
            }
        } else {
            return undefined
        }
    }

    public get() {
        if (proxylessMode) {
            return undefined
        }
        const proxy = this.proxies.shift()
        this.proxies.push(proxy)
        return proxy
    }
}
