import * as ipc from 'node-ipc'
import { defaultLogger, Logger } from './logger'

export interface IPCServerConfig {
    id: string
    retry: number
    silent: boolean
    appspace: string
    networkPort: number
    maxConnections: number
}

const defaultIPCConfig: IPCServerConfig = {
    id: 'server',
    retry: 50,
    silent: true,
    appspace: 'bopify.',
    networkPort: 6969,
    maxConnections: 5000
}

export class IPCServer {
    public server: any
    public logger: Logger = defaultLogger('IPC Server', this.silent)

    constructor(
        public silent = false,
        public config: IPCServerConfig = defaultIPCConfig
    ) {
        this.server = this.start()
    }

    public start(): any {
        Object.assign(ipc.config, this.config)
        ipc.serve(() => {
            this.logger.init(`Started IPC server: ${this.config.id}`)
        })
        ipc.server.start()
        return ipc.server
    }

    public emit(...args: any): IPCServer {
        this.server.emit(...args)
        return this
    }

    public on(event: string, callback: Function): IPCServer {
        this.server.on(event, callback)
        return this
    }

    public disconnect() {
        this.server.stop()
    }
}
