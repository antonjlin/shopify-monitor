import { StatsD } from 'hot-shots'
import { MonitorSource } from '../types/basetypes'
import { ProductNotification } from '../types/notificationtypes'

export class DatadogSender extends StatsD {
  constructor() {
    super()
  }

  getSourceTags(source: MonitorSource) {
    return [
      `monitor:${source.monitor}`,
      `endpoint:${source.endpoint}`,
      `domain:${source.site.domain}`
    ]
  }

  restockEvent(productNotification: ProductNotification, available: boolean) {
    const { product, source } = productNotification
    this.event(
      product.title,
      JSON.stringify(productNotification, null, 4),
      {},
      this.getSourceTags(source)
    )
    this.increment(
      'monitor.restock',
      this.getSourceTags(productNotification.source).concat([
        `available:${available}`
      ])
    )
  }

  ignoreRestockEvent(
    scrapeSizes: number[],
    restockSize: number,
    source: MonitorSource
  ) {
    this.event(
      'Ignored restock',
      JSON.stringify({ restockSize, scrapeSizes }, null, 4),
      {},
      this.getSourceTags(source)
    )
  }

  passwordEvent(isPasswordUp: boolean, source: MonitorSource) {
    this.event(
      `Password is ${isPasswordUp ? 'up' : 'down'}`,
      JSON.stringify(source, null, 4),
      {},
      this.getSourceTags(source)
    )
  }

  logProductListingsSize(size: number, source: MonitorSource) {
    // this.timing('monitor.scrape', size, this.getSourceTags(source))
  }

  logNumberOfMonitorsConnected(numConnected: number) {
    // this.timing('monitor.connected', numConnected)
  }

  logNumberOfClients(numClients: number) {
    // this.timing('websocket.clients', numClients)
  }

  logDynamicTimeout(timeout: number, source: MonitorSource) {
    // this.timing('monitor.timeout', timeout, this.getSourceTags(source))
  }

  logRequestPassword(statusCode: number, source: MonitorSource) {
    this.increment(
      'monitor.request',
      this.getSourceTags(source).concat([
        'result:password',
        `status:${statusCode}`
      ])
    )
  }

  logRequestBan(statusCode: number, code: string, source: MonitorSource) {
    this.increment(
      'monitor.request',
      this.getSourceTags(source).concat([
        'result:ban',
        `status:${statusCode}`,
        `error:${code}`
      ])
    )
  }

  logRequestTimeout(statusCode: number, time: number, source: MonitorSource) {
    const tags = this.getSourceTags(source).concat([
      'result:timeout',
      `status:${statusCode}`
    ])
    this.increment('monitor.request', tags)
    this.timing('monitor.request.responseTime', time, tags)
  }

  logRequestSuccess(
    statusCode: number,
    elapsedTime: number | undefined,
    source: MonitorSource
  ) {
    const tags = this.getSourceTags(source).concat([
      'result:success',
      `status:${statusCode}`
    ])
    this.increment('monitor.request', tags)
    elapsedTime &&
      this.timing('monitor.request.responseTime', elapsedTime, tags)
  }

  logRequestError(statusCode: number, code: string, source: MonitorSource) {
    this.increment(
      'monitor.request',
      this.getSourceTags(source).concat([
        'result:error',
        `status:${statusCode}`,
        `error:${code}`
      ])
    )
  }

  logTimerDelay(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:delay'])
    // )
  }

  logTimerRequest(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:request'])
    // )
  }

  logTimerComparison(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:comparison'])
    // )
  }

  logTimerNotification(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:notification'])
    // )
  }

  logTimerError(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:error'])
    // )
  }

  logTimerNothing(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:nothing'])
    // )
  }

  logTimerFailing(elapsed: number, source: MonitorSource) {
    // this.timing(
    //     'monitor.timer',
    //     elapsed,
    //     this.getSourceTags(source).concat(['doing:failing'])
    // )
  }
}
