import Cryptr from 'cryptr'
import uuid from 'uuid'
import WebSocket from 'ws'

import { DatadogSender } from './datadogsender'
import { defaultLogger } from './logger'

export class RestockWebsocket {
    public wss: any // WebSocket.Server ??error??
    public logger = defaultLogger('Websocket')
    public datadogSender = new DatadogSender()

    constructor(public port: number) {
        this.wss = new WebSocket.Server({ port })
        this.logger.init(`Websocket initialized at port ${8081}`)
        this.start()
    }

    public start() {
        this.wss.on('connection', () => {
            this.logger.success('Client connected')
        })
        setInterval(() => {
            this.datadogSender.logNumberOfClients(this.wss.clients.size)
        }, 5000)
    }

    public kill() {
        this.wss.close()
    }

    public async broadcast(msg: string | object) {
        if (typeof msg === 'object') {
            msg = JSON.stringify(msg)
        }
        this.logger.info('Broadcasting message...')
        this.wss.clients.forEach((client: any) => {
            if (client.readyState === WebSocket.OPEN) {
                client.send(msg)
            }
        })
    }
}
