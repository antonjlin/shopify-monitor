import chalk from 'chalk'
import fs from 'fs'
import moment from 'moment-timezone'

import {
    LogFunction,
    LoggerLevel,
    LogMessage,
    LogMessageFormatter,
} from '../types/loggertypes'

const currentTimestamp = (
    format = 'MM/DD hh:mm:ss A',
    timezone = 'America/Los_Angeles'
): string => {
    return moment()
        .tz(timezone)
        .format(format)
}

const stringify = (object: any): string => {
    if (object instanceof Error) {
        return object.stack || ''
    } else if (typeof object === 'object') {
        return JSON.stringify(object)
    }
    return String(object)
}

export class Logger {
    // accomodate for variable name log functions
    [key: string]: any

    constructor(
        public tags: string[],
        public folderPath: string,
        public levels: LoggerLevel[],
        public logMessageFormatter: LogMessageFormatter
    ) {
        levels.forEach((level) => {
            if (level.name === 'combined') {
                throw new Error(
                    '"combined" is a reserved keyword for logger levels'
                )
            }
            // this[level.name] = this.createLogFunction(
            //     tags,
            //     folderPath,
            //     level,
            //     logMessageFormatter
            // )
            this.createLogFunction(level)
        })
    }

    public addTag(tag: string): Logger {
        this.tags.push(tag)
        return this
    }

    public createLogFunction(level: LoggerLevel) {
        const logFunction: LogFunction = (
            message: any,
            extraTags: string[] = []
        ): void => {
            const timestamp = currentTimestamp()
            const logMessage: LogMessage = {
                timestamp,
                level,
                message: stringify(message),
                tags: this.tags.concat(extraTags)
            }
            const styledMessage = this.logMessageFormatter(logMessage, true)
            const rawMessage = this.logMessageFormatter(logMessage, false)
            const separateFilePath = `${this.folderPath}/${level.name}.txt`
            const combinedFilePath = `${this.folderPath}/combined.txt`

            const transporter = new LogTransporter(styledMessage, rawMessage)

            level.transports.console && transporter.console()
            level.transports.separateFile && transporter.file(separateFilePath)
            level.transports.combinedFile && transporter.file(combinedFilePath)
        }

        this[level.name] = logFunction.bind(this)
    }
}

// tslint:disable-next-line: max-classes-per-file
class LogTransporter {
    constructor(private styledMessage: string, private rawMessage: string) { }

    public console(): LogTransporter {
        console.log(this.styledMessage)
        return this
    }

    public file(path: string): LogTransporter {
        fs.appendFile(path, this.rawMessage + '\n', (err) => {
            this.handleWriteError(err, path)
        })
        return this
    }

    public handleWriteError(err: Error | null, filepath: string): void {
        if (err) {
            // TODO make more elegant
            if (
                err.message &&
                err.message.includes('no space left on device')
            ) {

                console.log('NO SPACE LEFT ON DEVICE')
                fs.unlink(filepath, () => {
                    console.log('cleared ' + filepath)
                })
            } else {
                console.log(err)
            }
        }
    }
}

// default logger
export const defaultLogger = (
    tags: string | string[],
    silent: boolean = false,
    folderPath: string = './logs'
): Logger => {
    if (!fs.existsSync(folderPath)) {
        fs.mkdirSync(folderPath)
    }

    if (typeof tags === 'string') {
        tags = [tags]
    }

    const logMessageFormatter: LogMessageFormatter = (
        logMessage: LogMessage,
        colored: boolean
    ) => {
        const { message, timestamp, level, tags } = logMessage
        if (colored) {
            return `${chalk.gray(`[${timestamp}]`)} ${tags
                .map((tag) => chalk.gray(`[${tag}]`))
                .join(' ')} ${chalk[level.color](level.name)} ${message}`
        }
        return `[${timestamp}] ${tags.map((tag) => `[${tag}]`).join(' ')} ${
            level.name
            } ${message}`
    }

    const defaultTransports = {
        separateFile: true,
        combinedFile: true,
        console: !silent
    }

    const levels: LoggerLevel[] = [
        {
            name: 'error',
            color: 'red',
            transports: defaultTransports
        },
        {
            name: 'init',
            color: 'greenBright',
            transports: defaultTransports
        },
        {
            name: 'success',
            color: 'green',
            transports: defaultTransports
        },
        {
            name: 'warn',
            color: 'yellow',
            transports: defaultTransports
        },
        {
            name: 'info',
            color: 'cyan',
            transports: defaultTransports
        },
        {
            name: 'restock',
            color: 'magenta',
            transports: defaultTransports
        },
        {
            name: 'password',
            color: 'magentaBright',
            transports: defaultTransports
        },
        {
            name: 'test',
            color: 'blue',
            transports: defaultTransports
        },
        {
            name: 'discount',
            color: 'cyanBright',
            transports: defaultTransports
        },
        {
            name: 'timeout',
            color: 'red',
            transports: {
                separateFile: false,
                combinedFile: false,
                console: false
            }
        },
        {
            name: 'ipc',
            color: 'yellow',
            transports: defaultTransports
        },
        {
            name: 'boost',
            color: 'cyan',
            transports: defaultTransports
        }
    ]

    return new Logger(tags, folderPath, levels, logMessageFormatter)
}
