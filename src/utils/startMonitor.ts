import { Site } from '../models/site'
import { TestConfig } from '../types/basetypes'
import { defaultLogger } from './logger'

const validateSite = (monitorClass: any, site: any) => {
    if (!site.displayName || !site.domain || !site.url) {
        throw new Error(`No valid site passed in to ${monitorClass.name}`)
    }
    return site
}

const validateTestConfig = (monitorClass: any, testConfig: any) => {
    if (
        testConfig.sendInitialScrape === undefined ||
        testConfig.testMode === undefined ||
        testConfig.silent === undefined
    ) {
        throw new Error(`No valid testMode passed in to ${monitorClass.name}`)
    }
    return testConfig
}

const validateProxies = (monitorClass: any, proxies: any) => {
    if (
        !Array.isArray(proxies) ||
        (proxies.length && typeof proxies[0] !== 'string')
    ) {
        throw new Error(`No valid proxies passed in to ${monitorClass.name}`)
    }
    return proxies
}

const validateLink = (monitorClass: any, link: any) => {
    if (typeof link !== 'string') {
        throw new Error(`No valid link passed in to ${monitorClass.name}`)
    }
    return link
}

export const startMonitor = (monitorClass: any, args: string[]) => {
    try {
        const site: Site = validateSite(monitorClass, JSON.parse(args[0]))
        const testConfig: TestConfig = validateTestConfig(
            monitorClass,
            JSON.parse(args[1])
        )
        const proxies: string[] = validateProxies(
            monitorClass,
            JSON.parse(args[2])
        )

        let monitor: any
        if (monitorClass.name === 'DirectLinkMonitor') {
            const link: string = validateLink(monitorClass, args[3])
            monitor = new monitorClass(site, proxies, link)
        } else {
            monitor = new monitorClass(site, proxies)
        }
        monitor.start(testConfig)
    } catch (err) {
        const logger = defaultLogger(['Monitor Start', monitorClass.name])
        logger.error(
            `Error starting: ${
                err instanceof Error ? err.toString() : JSON.stringify(err)
            }`
        )
    }
}
