import { GroupFormat, PotentialDropNotification } from '../types/notificationtypes'
import { GroupEmbed } from './groupembed'

export class PotentialDropEmbed extends GroupEmbed {
    constructor(
        public discNotification: PotentialDropNotification,
        format: GroupFormat
    ) {
        super(format)
        // if (format.discountEmbedOverride) {
        //     return format.discountEmbedOverride(this)
        // } else {
        //     this.createNotification()
        // }
        this.createNotification()
    }

    public createNotification() {
        const { productNumDifference, source } = this.discNotification

        this.setAuthor(source.site.domain, '', source.site.url)
            .setTitle('Potential Drop Alert')
            .setDescription('Products loaded on backend - potential drop incoming.')
            .setColor(this.format.color)
            .setTimestamp(undefined)
            .setFooter(
                this.footerText(source.monitor, undefined, productNumDifference.toString()),
                this.format.logo
            )
    }
}
