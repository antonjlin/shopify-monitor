import { Webhook } from '../types/notificationtypes'
import { DiscordEmbed } from './discordembed'
import { NotificationSender } from './notificationsender'

export class NotificationHookManager {
    public filteredSenders: NotificationSender[]
    public unfilteredSenders: NotificationSender[]
    public discountSenders: NotificationSender[]

    constructor(public webhook: Webhook) {
        this.filteredSenders = webhook.filtered.map(
            (webhook) => new NotificationSender(webhook)
        )
        this.unfilteredSenders = webhook.unfiltered.map(
            (webhook) => new NotificationSender(webhook)
        )
        this.discountSenders = webhook.discounts.map(
            (webhook) => new NotificationSender(webhook)
        )
    }

    public send(
        embed: DiscordEmbed,
        channel: 'unfiltered' | 'filtered' | 'discount',
        extraTags: string[] = []
    ) {
        if (channel === 'unfiltered') { this.sendUnfiltered(embed, extraTags) } else if (channel === 'filtered') { this.sendFiltered(embed, extraTags) } else if (channel === 'discount') { this.sendDiscount(embed, extraTags) }
    }

    public sendFiltered(embed: DiscordEmbed, extraTags: string[] = []) {
        if (this.filteredSenders.length > 0) {
            this.filteredSenders[0].add(embed, extraTags)
            const used = this.filteredSenders.shift()
            used && this.filteredSenders.push(used)
        }
    }

    public sendUnfiltered(embed: DiscordEmbed, extraTags: string[] = []) {
        if (this.unfilteredSenders.length > 0) {
            this.unfilteredSenders[0].add(embed, extraTags)
            const used = this.unfilteredSenders.shift()
            used && this.unfilteredSenders.push(used)
        }
    }

    public sendDiscount(embed: DiscordEmbed, extraTags: string[] = []) {
        if (this.discountSenders.length > 0) {
            this.discountSenders[0].add(embed, extraTags)
            const used = this.discountSenders.shift()
            used && this.discountSenders.push(used)
        }
    }
}
