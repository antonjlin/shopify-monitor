import moment from 'moment-timezone'

import { GroupFormat, PasswordNotification } from '../types/notificationtypes'
import { GroupEmbed } from './groupembed'

export class PasswordEmbed extends GroupEmbed {
    constructor(
        public passwordNotification: PasswordNotification,
        format: GroupFormat
    ) {
        super(format)
        if (format.passwordEmbedOverride) {
            return format.passwordEmbedOverride(this)
        } else {
            this.createNotification()
        }
    }

    public createNotification() {
        const { isPasswordUp, source } = this.passwordNotification

        const emoji = isPasswordUp ? '🔒' : '🔓'
        const message = `${emoji} Password page is ${
            isPasswordUp ? 'up' : 'down'
        }! ${emoji}`
        this.setAuthor(source.site.domain, '', source.site.url)
            .setTitle(message)
            .setTimestamp(undefined)
            .setColor(this.format.color)
            .setFooter(
                this.footerText(source.monitor, undefined),
                this.format.logo
            )
    }
}
