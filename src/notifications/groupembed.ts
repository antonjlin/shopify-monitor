import moment from 'moment-timezone'

import { Variant } from '../models/product'
import { DashboardExtraMeta } from '../types/dashboardtypes'
import { GroupFormat } from '../types/notificationtypes'
import { DiscordEmbed } from './discordembed'

export class GroupEmbed extends DiscordEmbed {
    constructor(public format: GroupFormat) {
        super(format.slack)
    }

    public descriptionText(
        priceStr: string | undefined,
        extraMeta: DashboardExtraMeta | undefined
    ): string | undefined {
        const description = []
        priceStr && description.push(priceStr)
        if (extraMeta) {
            if (extraMeta.shipsTo) {
                const { US, CA, AU, GB } = extraMeta.shipsTo
                if (US && !CA && !AU && !GB) {
                    description.push('US Shipping Only')
                } else if (!US) { description.push('No US Shipping') }
            }
            extraMeta.paypalOnly && description.push('PayPal Only')
            extraMeta.accountRequired && description.push('Account Required')
        }
        return description.length > 0 ? description.join('\n') : undefined
    }

    public footerText(
        monitor: String = '',
        keywordSet: undefined | string = undefined,
        additionalInfo: undefined | string = undefined
    ): string {
        const {
            label,
            includeMonitor,
            includeTimestamp,
            includeKeywords
        } = this.format.footer
        const footer = [
            label,
            ...(includeMonitor ? [monitor[0].toUpperCase()] : []),
            ...(additionalInfo !== undefined ? [additionalInfo] : []),
            ...(includeTimestamp
                ? [
                    moment()
                        .tz('America/Los_Angeles')
                        .format('hh:mm:ss A zz')
                ]
                : ''),
            ...(includeKeywords && keywordSet ? [keywordSet] : [])
        ]
        return footer.join(' • ')
    }

    public splitVariants(variants: Variant[]): string[] {
        const maxCharacters = 5000
        let usedCharacters = 0

        const variantStrings = variants.map(this.getVariantString.bind(this))
        const fieldValues: string[] = ['']
        let index = 0
        variantStrings.forEach((variantString) => {
            const { length } = variantString
            if (usedCharacters + length > maxCharacters) {
                return
            } else if (fieldValues[index].length + length > 1024) {
                fieldValues[index].trim()
                index++
                fieldValues.push('')
            }
            fieldValues[index] += variantString + '\n'
            usedCharacters += length
        })
        return fieldValues
    }

    public getVariantString(variant: Variant) {
        const { title, atcLink } = variant
        const { includeQuicktasks, getQTLink } = this.format.quicktasks
        const fieldValue =
            `${this.bold(this.hyperLink(title, atcLink))}` +
            (includeQuicktasks
                ? ` ${this.hyperLink('(QT)', getQTLink && getQTLink(atcLink, '1'))}`
                : '')
        return fieldValue
    }

    public createBotString(productLink: string) {
        if (this.format.quicktasks.includeQuicktasks) {
            return this.createQTBotString(productLink)
        }
        const bots = [
            {
                name: 'CYBER',
                link: `https://cybersole.io/dashboard/tasks?quicktask=${productLink}`
            },
            {
                name: 'DASHE',
                link: `https://api.dashe.io/v1/actions/quicktask?url=${productLink}`
            },
            {
                name: 'PD',
                link: `https://api.destroyerbots.io/quicktask?url=${productLink}`
            },
            {
                name: 'TKS',
                link: `https://thekickstationapi.com/quick-task.php?link=${productLink}&autostart=true`
            },
            {
                name: 'WHATBOT',
                link: `https://whatbot.club/redirect-qt?qt=whatbot://${productLink.replace(
                    'https://',
                    ''
                )}`
            }
        ]
        return bots.map((bot) => this.hyperLink(bot.name, bot.link)).join(' | ')
    }

    public createQTBotString(productLink: string) {
        const { getQTLink, configQTLink } = this.format.quicktasks
        return [
            ...[this.hyperLink('BOT 1', getQTLink && getQTLink(productLink, '1'))],
            ...[this.hyperLink('BOT 2', getQTLink && getQTLink(productLink, '2'))],
            ...(configQTLink ? [this.hyperLink('Setup', configQTLink)] : [])
        ].join(' | ')
    }
}
