import { GroupFormat } from '../types/notificationtypes'
import {
    popReportDiscountEmbed,
    popReportPasswordEmbed,
    popReportProductEmbed
} from './popreportoverride'

export const groupFormats: { [key: string]: GroupFormat } = {
    'Calicos': {
        slack: false,
        color: '0xc92f53',
        logo:
            'https://cdn.discordapp.com/attachments/488869395842465802/560315183344517128/54730453_417227919056148_3325896792825921536_n.png',
        footer: {
            label: 'Calicos',
            includeMonitor: true,
            includeTimestamp: true,
            includeKeywords: true
        },
        quicktasks: {
            includeQuicktasks: true,
            getQTLink: (url: string, botNum?: '1' | '2') =>
                `https://qt.calicos.io/?bot=${botNum}&input=${url}`,
            configQTLink: 'https://qt.calicos.io'
        }
    },
    'AIOMoji': {
        slack: false,
        color: '0x9B59B6',
        logo:
            'https://cdn.discordapp.com/attachments/488869395842465802/560315183344517128/54730453_417227919056148_3325896792825921536_n.png',
        footer: {
            label: 'AIOMoji x Calicos',
            includeMonitor: true,
            includeTimestamp: true,
            includeKeywords: true
        },
        botString: (
            url: string,
            hyperlink: (s: string, url: string) => string
        ) =>
            hyperlink(
                'AIOMOJI',
                `https://www.aiomoji.io/quicktask/add?storetype=shopify&url=${url}&storedomain=${url}`
            ),
        quicktasks: {
            includeQuicktasks: true,
            getQTLink: (url: string) =>
                `https://www.aiomoji.io/quicktask/add?storetype=shopify&url=${url}&storedomain=${url}`
        }
    },
    'Cybersole': {
        slack: false,
        color: '0x2ECC71',
        logo:
            'https://cdn.discordapp.com/attachments/488869395842465802/560315183344517128/54730453_417227919056148_3325896792825921536_n.png',
        footer: {
            label: 'Cyber x Calicos',
            includeMonitor: true,
            includeTimestamp: true,
            includeKeywords: true
        },
        botString: (
            url: string,
            hyperlink: (s: string, url: string) => string
        ) =>
            hyperlink(
                'CYBER',
                `https://cybersole.io/dashboard/tasks?quicktask=${url}`
            ),
        quicktasks: {
            includeQuicktasks: true,
            getQTLink: (url: string) =>
                `https://cybersole.io/dashboard/tasks?quicktask=${url}`
        }
    },
    'PopReport': {
        slack: true,
        color: '0x4cbff1',
        logo:
            'https://cdn.discordapp.com/attachments/488869395842465802/560315228252798979/g9GVqoRP_400x400.png',
        footer: {
            label: 'PopReport',
            includeMonitor: false,
            includeTimestamp: false,
            includeKeywords: false
        },
        productEmbedOverride: popReportProductEmbed,
        passwordEmbedOverride: popReportPasswordEmbed,
        discountEmbedOverride: popReportDiscountEmbed,
        quicktasks: {
            includeQuicktasks: false
        }
    },
    'Calicos Funko': {
        slack: false,
        color: '0xc92f53',
        logo:
            'https://cdn.discordapp.com/attachments/488869395842465802/560315183344517128/54730453_417227919056148_3325896792825921536_n.png',
        footer: {
            label: 'Calicos',
            includeMonitor: true,
            includeTimestamp: true,
            includeKeywords: true
        },
        quicktasks: {
            includeQuicktasks: true,
            getQTLink: (url: string, botNum?: '1' | '2') =>
                `https://qt.calicos.io/?bot=${botNum}&input=${url}`,
            configQTLink: 'https://qt.calicos.io'
        }
    },
    'FunkoGarage': {
        slack: false,
        color: '0xfff77d',
        logo:
            'https://cdn.discordapp.com/attachments/488869395842465802/560315158031892500/55557075_652812915177344_7598990812524838912_n.png',
        footer: {
            label: 'FunkoGarage',
            includeMonitor: true,
            includeTimestamp: true,
            includeKeywords: true
        },
        quicktasks: {
            includeQuicktasks: false
        }
    },
    'Reselligent': {
        slack: false,
        color: '0xffb000',
        logo:
            'https://cdn.discordapp.com/attachments/560124606501879811/560252808553824260/234234234.png',
        footer: {
            label: 'Reselligent',
            includeMonitor: true,
            includeTimestamp: true,
            includeKeywords: true
        },
        quicktasks: {
            includeQuicktasks: false
        }
    },
    'Balko': {
        slack: false,
        color: '0x2a2f3b',
        logo:
            'https://cdn.discordapp.com/attachments/567478033389191318/567478709406400522/LOGOwhite.png',
        footer: {
            label: 'Balko x Calicos',
            includeMonitor: false,
            includeTimestamp: true,
            includeKeywords: false
        },
        quicktasks: {
            includeQuicktasks: false
        },
        botString: (
            url: string,
            hyperlink: (s: string, url: string) => string
        ) => hyperlink('BALKO', `http://localhost:6776/?url=${url}`)
    },
    'Rihbey': {
        slack: false,
        color: '0xde73a6',
        logo:
            'https://cdn.discordapp.com/attachments/439530002615107584/590178403550756885/image1.png',
        footer: {
            label: 'RihBey',
            includeMonitor: false,
            includeTimestamp: true,
            includeKeywords: false
        },
        quicktasks: {
            includeQuicktasks: false
        },
        botString: () => ''
    }
}
