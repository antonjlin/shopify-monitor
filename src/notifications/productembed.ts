import { GroupFormat, ProductNotification } from '../types/notificationtypes'
import { GroupEmbed } from './groupembed'

export class ProductEmbed extends GroupEmbed {
    constructor(
        public productNotification: ProductNotification,
        format: GroupFormat
    ) {
        super(format)
        if (format.productEmbedOverride) {
            return format.productEmbedOverride(this)
        } else {
            this.createNotification()
        }
    }

    public createNotification() {
        const { source, product, keywordSet } = this.productNotification
        const site = source.site
        this.setAuthor(site.domain, '', site.url)
            .setTitle(product.title)
            .setURL(product.link)
            .setColor(this.format.color)
            .setTimestamp(undefined)
            .setFooter(
                this.footerText(source.monitor, keywordSet),
                this.format.logo
            )

        product.image && this.setThumbnail(product.image)

        const priceStr = product.price ? product.price.priceStr : undefined
        const description = this.descriptionText(priceStr, site.extraMeta)
        description && this.setDescription(description)

        if (product.variants) {
            const availableVariants = product.variants.filter(
                (variant) => variant.available
            )
            if (availableVariants.length > 0) {
                this.splitVariants(availableVariants).forEach(
                    (fieldValue, idx) => {
                        this.addField(idx === 0 ? 'ATC' : '', fieldValue, true)
                    }
                )
            } else {
                this.setDescription('OUT OF STOCK')
            }
        }

        const botString = this.format.botString
            ? this.format.botString(product.link, this.hyperLink.bind(this))
            : this.createBotString(product.link)
        botString && this.addField('QT', botString, false)
    }
}
