import { DiscountNotification, GroupFormat } from '../types/notificationtypes'
import { GroupEmbed } from './groupembed'

export class DiscountEmbed extends GroupEmbed {
    constructor(
        public discNotification: DiscountNotification,
        format: GroupFormat
    ) {
        super(format)
        if (format.discountEmbedOverride) {
            return format.discountEmbedOverride(this)
        } else {
            this.createNotification()
        }
    }

    public createNotification() {
        const { discountInfo, source } = this.discNotification
        const site = source.site
        const product = discountInfo.product

        this.setAuthor(site.domain, '', site.url)
            .setTitle(product.title)
            .setURL(product.link)
            .setColor(this.format.color)
            .setTimestamp(undefined)
            .setFooter(
                this.footerText(source.monitor, undefined),
                this.format.logo
            )

        const priceOg = Number(discountInfo.originalPrice).toFixed(2)
        const newPrice = Number(discountInfo.newPrice).toFixed(2)
        this.setDescription(
            `$${this.cross(priceOg)} $${newPrice} (${Number(
                discountInfo.percentDiscount
            ).toFixed(2)}% OFF)`
        )

        product.image && this.setThumbnail(product.image)

        if (product.variants) {
            const availableVariants = product.variants.filter(
                (variant) => variant.available
            )
            if (availableVariants.length > 0) {
                this.splitVariants(availableVariants).forEach(
                    (fieldValue, idx) => {
                        this.addField(idx === 0 ? 'ATC' : '', fieldValue, true)
                    }
                )
            } else {
                this.setDescription('OUT OF STOCK')
            }
        }

        this.addField(
            'QT',
            this.format.botString
                ? this.format.botString(product.link, this.hyperLink.bind(this))
                : this.createBotString(product.link),
            false
        )
    }
}
