import { GroupFormat, SubDomainNotification } from '../types/notificationtypes'
import { GroupEmbed } from './groupembed'

export class SubDomainEmbed extends GroupEmbed {
    constructor(
        public discNotification: SubDomainNotification,
        format: GroupFormat
    ) {
        super(format)
        this.createNotification()
    }

    public createNotification() {
        const { subdomain, source } = this.discNotification

        this.setTitle('Subdomain Monitor')
            .setDescription(
                this.hyperLink(subdomain, `https://${subdomain}`) + ' added to ' +
                this.hyperLink(source.site.url, `https://${source.site.url}`))
            .setColor(this.format.color)
            .setTimestamp(undefined)
            .setFooter(
                this.footerText(source.monitor, undefined),
                this.format.logo
            )
    }
}
