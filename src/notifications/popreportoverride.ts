import { DiscountEmbed } from './discountembed'
import { PasswordEmbed } from './passwordembed'
import { ProductEmbed } from './productembed'

export const popReportProductEmbed = (productEmbed: ProductEmbed) => {
    const { source, product } = productEmbed.productNotification
    productEmbed
        .setAuthor(source.site.domain, '', source.site.url)
        .setTitle(product.title)
        .setURL(product.link)
        .setColor(productEmbed.format.color)
        .setTimestamp(undefined)
        .setFooter(productEmbed.footerText(), productEmbed.format.logo)

    product.image && productEmbed.setThumbnail(product.image)
    product.price &&
        productEmbed.addField('Price', product.price.priceStr, false)

    if (product.variants) {
        const availableVariants = product.variants.filter(
            (variant) => variant.available
        )
        if (availableVariants.length === 1) {
            productEmbed.addAction(
                'Add to Cart',
                availableVariants[0].atcLink || ''
            )
        } else if (availableVariants.length > 1) {
            productEmbed
                .addField(
                    'Add to Cart',
                    availableVariants
                        .filter((_, idx) => idx % 2 == 0)
                        .map((variant) =>
                            productEmbed.bold(
                                productEmbed.hyperLink(
                                    variant.title,
                                    variant.atcLink
                                )
                            )
                        )
                        .join('\n'),
                    true
                )
                .addField(
                    '-',
                    availableVariants
                        .filter((_, idx) => idx % 2 == 1)
                        .map((variant) =>
                            productEmbed.bold(
                                productEmbed.hyperLink(
                                    variant.title,
                                    variant.atcLink
                                )
                            )
                        )
                        .join('\n'),
                    true
                )
        }
    }

    return productEmbed
}

export const popReportPasswordEmbed = (passwordEmbed: PasswordEmbed) => {
    const { isPasswordUp, source } = passwordEmbed.passwordNotification

    const message = isPasswordUp
        ? 'Password page is up.'
        : 'Password page is down.'
    passwordEmbed
        .setAuthor(source.site.domain, '', source.site.url)
        .setTitle(message)
        .setTimestamp(undefined)
        .setColor(passwordEmbed.format.color)
        .setFooter(
            passwordEmbed.footerText(source.monitor, undefined),
            passwordEmbed.format.logo
        )
    return passwordEmbed
}

export const popReportDiscountEmbed = (discountEmbed: DiscountEmbed) => {
    const { discountInfo, source } = discountEmbed.discNotification
    const product = discountInfo.product

    discountEmbed
        .setAuthor(source.site.domain, '', source.site.url)
        .setTitle(product.title)
        .setURL(product.link)
        .setColor(discountEmbed.format.color)
        .setTimestamp(undefined)
        .setFooter(
            discountEmbed.footerText(source.monitor, undefined),
            discountEmbed.format.logo
        )

    const priceOg = Number(discountInfo.originalPrice).toFixed(2)
    const newPrice = Number(discountInfo.newPrice).toFixed(2)
    discountEmbed.addField(
        'Price',
        `$${discountEmbed.cross(priceOg)} → $${newPrice} (${Number(
            discountInfo.percentDiscount
        ).toFixed(2)}% discount)`
    )

    product.image && discountEmbed.setThumbnail(product.image)

    if (product.variants) {
        const availableVariants = product.variants.filter(
            (variant) => variant.available
        )
        if (availableVariants.length === 1) {
            discountEmbed.addAction(
                'Add to Cart',
                availableVariants[0].atcLink || ''
            )
        } else if (availableVariants.length > 1) {
            discountEmbed
                .addField(
                    'Add to Cart',
                    availableVariants
                        .filter((_, idx) => idx % 2 == 0)
                        .map((variant) =>
                            discountEmbed.bold(
                                discountEmbed.hyperLink(
                                    variant.title,
                                    variant.atcLink
                                )
                            )
                        )
                        .join('\n'),
                    true
                )
                .addField(
                    '-',
                    availableVariants
                        .filter((_, idx) => idx % 2 == 1)
                        .map((variant) =>
                            discountEmbed.bold(
                                discountEmbed.hyperLink(
                                    variant.title,
                                    variant.atcLink
                                )
                            )
                        )
                        .join('\n'),
                    true
                )
        }
    }

    return discountEmbed
}
