import { WebClient } from '@slack/client'
import EventEmitter from 'events'
import rp from 'request-promise'

import { defaultLogger, Logger } from '../utils/logger'
import { DiscordEmbed } from './discordembed'

const slackToken = 'xoxb-467323340663-467176976499-bMeAd5lZ2e9gk1aiZ4NHtgky'

export class NotificationSender {
    public emitter: EventEmitter = new EventEmitter()
    public logger: Logger = defaultLogger('Discord Sender', this.silent)
    public delay: number = 0
    public queue: Array<{ embed: DiscordEmbed; extraTags: string[] }> = []
    public running: boolean = false
    public slack: boolean = false
    public slackClient = new WebClient(slackToken)

    /**
     * Create a new notifier.
     * @param {string} hook - The webhook to send to.
     */
    constructor(
        public webhook: string,
        public silent: boolean = false,
        public testEmitter: EventEmitter | null = null
    ) {
        this.slack = !this.webhook.includes('discord')
        this.run()
        testEmitter && testEmitter.emit('initialized')
    }

    /**
     * Adds an embed to queue
     * @param {object} embed The embed to add
     */
    public add(embed: DiscordEmbed, extraTags: string[] = []) {
        this.emitter.emit('new.notification', {
            embed,
            extraTags
        })
    }

    /**
     * Runs the queue sending, and pausing when rate limited
     */
    public run() {
        this.emitter.on('new.notification', (args) => {
            const { embed, extraTags } = args
            if (!(embed instanceof DiscordEmbed)) {
                throw new Error(
                    'Discord sender event emitter received non Embed arguments'
                )
            }

            if (!this.running) {
                this.running = true
                this.slack
                    ? this.sendSlack(embed, extraTags)
                    : this.sendDiscord(embed, extraTags)
            } else {
                this.queue.push(args)
            }
        })

        this.emitter.on('finished.send', () => {
            if (this.queue.length > 0) {
                const next = this.queue.shift()
                if (next) {
                    const { embed, extraTags } = next

                    setTimeout(
                        () =>
                            embed && this.slack
                                ? this.sendSlack(embed, extraTags)
                                : this.sendDiscord(embed, extraTags),
                        this.delay
                    )
                } else {
                    this.emitter.emit('finished.send')
                }
            } else {
                this.running = false
                this.testEmitter && this.testEmitter.emit('finished')
            }
        })
    }

    public async sendSlack(embed: DiscordEmbed, extraTags: string[] = []) {
        this.slackClient.chat
            .postMessage(embed.toSlack(this.webhook))
            .then(() => {
                this.logger.info('Slack sent', extraTags)
                this.emitter.emit('finished.send')
            })
            .catch((err) => {
                this.logger.error(`Slack error: ${err}`)
                this.emitter.emit('finished.send')
            })
    }

    public async sendDiscord(
        embed: DiscordEmbed,
        extraTags: string[] = [],
        nonRateLimitErrors: number = 0
    ): Promise<void> {
        if (nonRateLimitErrors >= 3) {
            this.logger.error(
                `Discord failed to send after ${nonRateLimitErrors} tries.`,
                extraTags
            )
            this.logger.error(embed)
            this.emitter.emit('finished.send')
            return
        }
        const options: rp.Options = {
            method: 'POST',
            url: this.webhook,
            body: { embeds: [embed] },
            json: true
        }
        try {
            await rp(options)
            this.logger.success('Discord sent.', extraTags)
            this.emitter.emit('finished.send')
        } catch (discordError) {
            const { error } = discordError
            if (
                discordError.response &&
                discordError.response.statusCode === 429
            ) {
                const retryDelay = error.retry_after ? error.retry_after : 1000
                this.logger.error(
                    `Discord rate limited, retrying in ${retryDelay} ms`
                )
                setTimeout(
                    () =>
                        this.sendDiscord(embed, extraTags, nonRateLimitErrors),
                    retryDelay
                )
            } else {
                this.logger.error(`Discord error: ${JSON.stringify(error)}`)
                this.sendDiscord(embed, extraTags, nonRateLimitErrors + 1)
            }
        }
    }
}
