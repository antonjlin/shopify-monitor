import { AttachmentAction, ChatPostMessageArguments } from '@slack/client'
import {
    Author,
    Embed,
    Field,
    Footer,
    Image,
    Thumbnail,
} from '../types/discordtypes'

export class DiscordEmbed {
    public embed: Embed = {}
    public title: string = ''
    public url: string = ''
    public description: string = ''
    public color: number = 0
    public timestamp: string = new Date().toISOString()
    public fields: Field[] = []
    public author: Author = { name: '', icon_url: '', url: '' }
    public footer: Footer = { text: '', icon_url: '' }
    public thumbnail: Thumbnail = { url: '' }
    public image: Image = { url: '' }
    public slackActions: AttachmentAction[] = []

    /**
     * Create a new embed object.
     */
    constructor(public slack = false) { }

    /**
     * takes data and converts it to a string
     * @param {any} data data to normalize to string
     * @returns {object} the normalized string
     */
    public normalize(data: string | string[] | number[]): string {
        if (typeof data === 'string') {
            return data
        }
        if (data instanceof Array) {
            return data.join('\n')
        }
        return String(data)
    }

    /**
     * takes text and a url and returns a hyperlink
     * @param {string} text text to display
     * @param {string | undefined} url url to link to
     * @returns {object} the normalized object
     */
    public hyperLink(text: string, url: string | undefined): string {
        if (url === undefined) {
            return `[${text}]`
        } else {
            url = this.fixLink(url)
            if (this.slack) {
                return `<${url}|${text}>`
            }
            return `[${text}](${url})`
        }
    }

    public bold(text: string): string {
        if (this.slack) {
            return `*${text}*`
        }
        return `**${text}**`
    }

    public cross(text: string): string {
        if (this.slack) {
            return `~${text}~`
        }
        return `~~${text}~~`
    }

    public fixLink(link: string) {
        if (link.slice(0, 4) !== 'http') {
            link = 'https:' + link
        }
        return link
    }

    public addAction(text: string, url: string, type: 'button' | 'select' = 'button') {
        if (!this.slack) {
            throw new Error('Cannot add button to discord notification!')
        }
        this.slackActions.push({ text, url, type, name: text })
    }

    /**
     * removes all empty string keys or null values from any object
     * @param {any} obj object to normalized
     * @returns {object} the normalized object
     */
    public normalizeObject(obj: any): Object {
        for (const key in obj) {
            if (!obj[key]) {
                delete obj[key]
            }
            if (
                obj[key] &&
                (obj[key] instanceof Object && !(obj[key] instanceof Array))
            ) {
                obj[key] = this.normalizeObject(obj[key])
            }
        }
        return obj
    }

    /**
     * Sets the title of the embed.
     * @param {any} title The title
     * @returns {DiscordEmbed} This embed
     */
    public setTitle(title: any): DiscordEmbed {
        title = this.normalize(title)
        if (title.length > 256) {
            title = title.substring(0, 256)
        }
        this.title = title
        return this
    }

    /**
     * Sets the description of the embed.
     * @param {any} description The description
     * @returns {DiscordEmbed} This embed
     */
    public setDescription(description: any): DiscordEmbed {
        description = this.normalize(description)
        if (description.length > 2048) {
            description = description.substring(0, 256)
        }
        this.description = description
        return this
    }

    /**
     * Sets the URL of the embed.
     * @param {string} url The URL
     * @returns {DiscordEmbed} This embed
     */
    public setURL(url: string): DiscordEmbed {
        this.url = url
        return this
    }

    /**
     * Sets the color of the embed.
     * @param {number | string} color The color of the embed
     * @returns {DiscordEmbed} This embed
     */
    public setColor(color: any): DiscordEmbed {
        this.color = parseInt(color)
        return this
    }

    /**
     * Sets the author of the embed.
     * @param {any} name The name of the author
     * @param {string} [icon] The icon URL of the author
     * @param {string} [url] The URL of the author
     * @returns {DiscordEmbed} This embed
     */
    public setAuthor(name: any, icon: string, url: string): DiscordEmbed {
        this.author = { name: this.normalize(name), icon_url: icon, url }
        return this
    }

    /**
     * Sets the timestamp of the embed.
     * @param {any} [timestamp] optional, default will be ```Date.now()```
     * @returns {DiscordEmbed} This embed
     */
    public setTimestamp(timestamp: any): DiscordEmbed {
        if (timestamp instanceof Date) {
            timestamp = timestamp.toISOString()
        }
        this.timestamp = timestamp
        return this
    }

    /**
     * Adds a field to the embed.
     * @param {any} name name of the field
     * @param {any} value value of the field
     * @param {boolean} [inline=false] whether or not the field is inline
     * @returns {DiscordEmbed} This embed
     */
    public addField(
        name: string,
        value: string,
        inline: boolean = false
    ): DiscordEmbed {
        if (this.fields.length >= 25) {
            this.fields = this.fields.slice(0, 24)
        }
        name = this.normalize(name)
        value = this.normalize(value)
        if (name.length > 256) {
            name = name.substring(0, 256)
        }
        if (value.length > 1024) {
            value = value.substring(0, 1024)
        }
        if (!/\S/.test(name)) {
            name = '\u200B'
        }
        if (!/\S/.test(value)) {
            value = '\u200B'
        }
        this.fields.push({ name, value, inline })
        return this
    }

    /**
     * Adds a blank field to the embed
     * @param {boolean} [inline=false] whether or not the field is inline
     * @returns {DiscordEmbed} This embed
     */
    public addBlankField(inline: false): DiscordEmbed {
        return this.addField('\u200B', '\u200B', inline)
    }

    /**
     * Set the thumbnail of the embed.
     * @param {string} url the url of the thumbnail
     * @returns {DiscordEmbed} This embed
     */
    public setThumbnail(url: string): DiscordEmbed {
        this.thumbnail = { url: this.fixLink(url) }
        return this
    }

    /**
     * Set the image of the embed.
     * @param {string} url the url of the image
     * @returns {DiscordEmbed} This embed
     */
    public setImage(url: string): DiscordEmbed {
        this.image = { url }
        return this
    }

    /**
     * Sets the footer of the embed.
     * @param {any} text The text of the footer
     * @param {string} [icon] the icon url of the footer
     * @returns {DiscordEmbed} This embed
     */
    public setFooter(text: string, icon: string): DiscordEmbed {
        text = this.normalize(text)
        if (text.length > 2048) {
            throw new RangeError(
                'DiscordEmbed footer text may not exceed 2048 characters.'
            )
        }
        this.footer = { text, icon_url: icon }
        return this
    }

    /**
     * Builds the embed so that it can be sent in a post request
     * @returns {Object} the JSON POST ready embed
     */
    public build(): Embed {
        return this.normalizeObject({
            title: this.title,
            type: 'rich',
            description: this.description,
            url: this.url,
            timestamp: this.timestamp ? new Date(this.timestamp) : null,
            color: this.color,
            fields: this.fields
                ? this.fields.map((field) => ({
                    name: field.name,
                    value: field.value,
                    inline: field.inline
                }))
                : null,
            thumbnail: this.thumbnail ? { url: this.thumbnail.url } : null,
            image: this.image
                ? {
                    url: this.image.url
                }
                : null,
            author: this.author
                ? {
                    name: this.author.name,
                    url: this.author.url,
                    icon_url: this.author.icon_url
                }
                : null,
            footer: this.footer
                ? {
                    text: this.footer.text,
                    icon_url: this.footer.icon_url
                }
                : null
        })
    }

    public toSlack(channel: string): ChatPostMessageArguments {
        if (!this.slack) {
            throw new Error('Tried to convert discord notification to slack!')
        }
        return {
            channel,
            text: '',
            mrkdwn: true,
            attachments: [
                {
                    fallback: this.title,
                    color: '#' + this.color.toString(16),
                    author_name: this.author.name,
                    author_link: this.author.url,
                    title: this.title,
                    title_link: this.url,
                    text: this.description,
                    thumb_url: this.thumbnail.url,
                    image_url: this.image.url,
                    footer: this.footer.text,
                    footer_icon: this.footer.icon_url,
                    fields: this.fields.map((field) => {
                        return {
                            title: field.name,
                            value: field.value,
                            short: field.inline
                        }
                    }),
                    actions: this.slackActions
                }
            ]
        }
    }
}
