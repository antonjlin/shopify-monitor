import { subdomainsites } from './data/subdomainsites'
import {
    testGroups,
    testProxies,
    testSites,
    testWebhooks,
} from './data/testdata'
import { Site } from './models/site'
import { ScrapeLinkMessage, TestConfig } from './types/basetypes'
import { BoostConfig } from './types/boosttypes'
import {
    DashboardGroup,
    DashboardKeywords,
    DashboardSite,
    DashboardWebhook,
} from './types/dashboardtypes'
import {
    DiscountNotification,
    PasswordNotification,
    PotentialDropNotification,
    ProductNotification,
    SubDomainNotification,
} from './types/notificationtypes'
import { bopifyDashboard } from './utils/bopifydashboard'
import { ChildProcesses } from './utils/childprocesses'
import { DatadogSender } from './utils/datadogsender'
import { IPCServer } from './utils/ipcserver'
import { defaultLogger } from './utils/logger'
import { NotificationManager } from './utils/notificationmanager'
import { allMonitorsConnected, sendBoost } from './utils/sendGithubNotification'

export class App {
    public ipc = new IPCServer(this.testConfig.silent)
    public childProcesses: ChildProcesses = new ChildProcesses(this.testConfig.silent)
    public loggingInterval: NodeJS.Timeout | null = null
    public logger = defaultLogger('Main', this.testConfig.silent)
    public notificationManager: NotificationManager | null = null
    public numMonitorsConnected: number = 0
    public subdomainSites: Site[] = []
    public datadogSender = new DatadogSender()

    constructor(
        public testConfig: TestConfig,
        public proxies: string[] = [],
        public groups: DashboardGroup[] = [],
        public sites: DashboardSite[] = [],
        public keywords: DashboardKeywords[] = [],
        public webhooks: DashboardWebhook[] = []
    ) { }

    public async start() {
        await this.getDatabaseInfo()
        await this.registerIPCServer()
        // TODO: maxThreads
        for (const site of this.sites) {
            this.launchSite(site)
            await new Promise(resolve => setTimeout(resolve, 200))
        }
        // this.sites.forEach(site => this.launchSite(site))
        this.launchShopifyLinkThread()
        this.launchPotentialDropMonitor()
        this.lauchSubDomainMonitor()
        this.loggingInterval = setInterval(() => {
            this.logger.info(`${this.childProcesses.length} processes running`)
            this.datadogSender.logNumberOfMonitorsConnected(
                Math.min(
                    this.numMonitorsConnected,
                    this.childProcesses.length -
                    this.childProcesses.numDisconnected
                )
            )
            this.syncKeywordsAndWebhooks()
        }, 15000)
    }

    public async kill() {
        this.logger.info('Killing app')
        this.loggingInterval && clearInterval(this.loggingInterval)
        this.childProcesses.killAll()
        this.ipc.disconnect()
        this.notificationManager && this.notificationManager.ws.kill()
    }

    public async syncKeywordsAndWebhooks() {
        const startRequest = Date.now()
        const [webhooks, keywords] = await Promise.all([
            bopifyDashboard.getWebhooks(),
            bopifyDashboard.getKeywords()
        ])
        if (!this.testConfig.testMode) { this.webhooks = webhooks }
        this.keywords = keywords
        const startSync = Date.now()
        this.notificationManager &&
            this.notificationManager.syncKeywordsAndWebhooks(
                this.keywords,
                this.webhooks
            )
        this.logger.info(
            `Synced keywords and webhooks in ${Date.now() -
            startRequest}/${Date.now() - startSync} ms`
        )
    }

    public async getDatabaseInfo() {
        if (!this.testConfig.testMode) {
            this.groups = await bopifyDashboard.getGroups()
            this.sites = await bopifyDashboard.getSites()
            this.webhooks = await bopifyDashboard.getWebhooks()
            this.proxies = await bopifyDashboard.getProxies()
        }
        this.keywords = await bopifyDashboard.getKeywords()
        this.notificationManager = new NotificationManager(
            this.groups,
            this.sites,
            this.keywords,
            this.webhooks
        )
        this.logger.info(`Retrieved ${this.sites.length} sites from dashboard`)
    }

    public async registerIPCServer(): Promise<void> {
        this.ipc
            .on('monitor.connected', (data: any, socket: any) => {
                this.ipc.logger.ipc(`Monitor connected to IPC: ${data}`)
                this.incrementMonitors()
            })
            .on('link.thread.connected', (_: any, socket: any) => {
                this.ipc.logger.ipc(`Link thread connected to IPC`)
                this.ipc.on('scrape.link', (data: ScrapeLinkMessage) => {
                    this.scrapeLink(data, socket)
                })
                this.incrementMonitors()
            })
            .on('potential.monitor.connected', (_: any, socket: any) => {
                this.ipc.logger.ipc(`Potential Monitor connected to IPC`)
                this.ipc.emit(socket, 'proxy', this.proxies)
                this.ipc.on('potential.add', (data: any) => {
                    this.ipc.emit(socket, 'potential.add', data.source.site)
                })
                this.ipc.on('potential.remove', (data: any) => {
                    console.log(data)
                    this.ipc.emit(socket, 'potential.remove', data.source.site)
                })
                this.incrementMonitors()
            })
            .on('subdomain.monitor.connected', (_: any, socket: any) => {
                this.ipc.logger.ipc(`SubDomain Monitor connected to IPC`)
                this.ipc.emit(socket, 'proxy', this.proxies)
                this.incrementMonitors()
            })
            .on(
                'restock',
                (data: ProductNotification) =>
                    this.notificationManager &&
                    this.notificationManager.sendRestockNotification(data)
            )
            .on(
                'password.update',
                (data: PasswordNotification) =>
                    this.notificationManager &&
                    this.notificationManager.sendPasswordNotification(data)
            )
            .on(
                'discount.update',
                (data: DiscountNotification) =>
                    this.notificationManager &&
                    this.notificationManager.sendDiscountNotification(data)
            )
            .on(
                'potential.update',
                (data: PotentialDropNotification) =>
                    this.notificationManager &&
                    this.notificationManager.sendPotentialDropNotification(data)
            )
            .on(
                'subdomain.update',
                (data: SubDomainNotification) =>
                    this.notificationManager &&
                    this.notificationManager.sendSubDomainNotification(data)
            )
            .on('boost', (data: BoostConfig) => {
                const { threadMultiplier, domain, duration } = data
                if (threadMultiplier && domain && duration) {
                    this.logger.boost(
                        `Activating boost with config ${JSON.stringify(data)}`
                    )
                    const sites = this.sites.filter(
                        (site) =>
                            site.meta.domain === domain ||
                            site.meta.myshopify_domain === domain
                    )
                    if (sites.length === 0) {
                        this.logger.error('COULDNT FIND SITE FOR: ' + domain)
                        return
                    }
                    this.logger.boost(
                        `Boosting ${JSON.stringify(
                            sites.map((site) => site.meta.domain)
                        )} with ${threadMultiplier} threads for ${duration} minutes`
                    )
                    sendBoost(data)
                    for (let i = 0; i < threadMultiplier; i++) {
                        sites.forEach((site) => {
                            this.launchSite(site, duration * 1000 * 60)
                        })
                    }
                } else {
                    this.logger.error(
                        `Invalid boost config! ${JSON.stringify(data)}`
                    )
                }
            })
    }

    public incrementMonitors() {
        this.numMonitorsConnected += 1
        if (this.numMonitorsConnected === this.childProcesses.length) {
            this.logger.success(
                `All ${this.numMonitorsConnected} monitors connected!`
            )
            this.testConfig.testMode ||
                allMonitorsConnected(this.numMonitorsConnected)
        }
    }

    public launchSite(
        dashboardSite: DashboardSite,
        timeoutMilliseconds: number | undefined = undefined
    ) {
        const site: Site = Site.fromDashboard(
            dashboardSite,
            dashboardSite.useMyShopify
        )
        const { meta, productLinks, monitor } = dashboardSite
        const url = meta.url
        const args: any[] = [
            JSON.stringify(site),
            JSON.stringify(this.testConfig),
            JSON.stringify(this.proxies)
        ]

        monitor.atom &&
            this.childProcesses.launch(
                './build/monitors/atommonitor.js',
                args,
                timeoutMilliseconds
            )
        monitor.sitemap &&
            this.childProcesses.launch(
                './build/monitors/sitemapmonitor.js',
                args,
                timeoutMilliseconds
            )
        monitor.json &&
            this.childProcesses.launch(
                './build/monitors/productjsonmonitor.js',
                args,
                timeoutMilliseconds
            )

        url.includes('kith') &&
            this.childProcesses.launch(
                './build/monitors/htmlmonitor.js',
                args,
                timeoutMilliseconds
            )

        url.includes('kith') &&
            this.childProcesses.launch(
                './build/monitors/mondayprogrammonitor.js',
                args,
                timeoutMilliseconds
            )

        url.includes('doverstreetmarket') &&
            this.childProcesses.launch(
                './build/monitors/dsmmonitor.js',
                args,
                timeoutMilliseconds
            )
        url.includes('yeezysupply') &&
            this.childProcesses.launch(
                './build/monitors/yeezysupplymonitor.js',
                args,
                timeoutMilliseconds
            )

        url.includes('yeezysupply') &&
            this.childProcesses.launch(
                './build/monitors/ysbackupmonitor.js',
                args,
                timeoutMilliseconds
            )
        productLinks.forEach((link: string) => {
            this.childProcesses.launch(
                './build/monitors/directlinkmonitor.js',
                args.concat(link),
                timeoutMilliseconds
            )
        })
        // Uncomment once subdomain boolean toggle is added to dash
        // monitor.subdomain &&
        //     this.subdomainSites.push(site)

        // remove \/ after that /\
        if (subdomainsites.indexOf(site.domain) > -1) {
            this.subdomainSites.push(site)
        }
    }

    public launchShopifyLinkThread() {
        this.childProcesses.launch('./build/monitors/linkthread.js', [
            JSON.stringify(this.proxies)
        ])
    }

    public launchPotentialDropMonitor() {
        this.childProcesses.launch(
            './build/monitors/potentialdropmonitor.js',
            []
        )
    }

    public lauchSubDomainMonitor() {
        this.childProcesses.launch('./build/monitors/subdomainmonitor.js', [
            JSON.stringify(this.subdomainSites)
        ])
    }

    public scrapeLink(message: ScrapeLinkMessage, socket: any) {
        const { link, site } = message
        const domain = site.domain
        try {
            const site = this.sites.filter(
                (site) =>
                    site.meta.domain === domain ||
                    site.meta.myshopify_domain === domain
            )[0]
            if (site === undefined) {
                this.logger.error('COULDNT MATCH UP SITES for ' + link)
                this.logger.error(JSON.stringify(site))
                return
            }
            const meta = site.meta

            if (meta) {
                this.ipc.emit(socket, 'scrape.link', {
                    link,
                    site: Site.fromDashboard(site)
                })
            } else {
                this.logger.error(`No meta found for ${link}`)
            }
        } catch (err) {
            this.logger.error(err)
        }
    }
}

const processArgs = process.argv.slice(2)
const testConfig: TestConfig =
    processArgs[0] === 'true'
        ? { testMode: true, sendInitialScrape: false, silent: false }
        : { testMode: false, sendInitialScrape: false, silent: false }

const app = new App(
    testConfig,
    testProxies,
    testGroups,
    testSites,
    [],
    testWebhooks
)
app.start()

process.on('SIGINT', () => {
    app.kill()
    process.exit()
})

process.on('exit', () => {
    app.kill()
    process.exit()
})

process.on('uncaughtException', (e) => {
    console.log(e)
    // TODO: handle this better... send notif to discord?
    // app.kill()
    // process.exit()
})
