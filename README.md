# Vital Orb

Vital Orb is an intelligent and distributed shopify monitor. Vital orb utilizes multiple processes to run multiple monitor instances on various endpoints per site.

![Diagram](diagram.png)

### Branches
`master`: current production branch - hosted on GCP Compute. Auto-deploy is active on this branch so make sure your code is 100% bug free.

### Usage:
Production: `npm start`

Development: `npm run test`

Auto compiler: `npm run compiler`

### Conventions
This repo follows the standard [Tranton Conventions](https://github.com/TrantonLLC/conventions). Make sure your IDE follows the specified setup.

### Related Projects
This repo pulls from the [Bopify Dashboard](https://github.com/TrantonLLC/bopify-main) API.

### Other Resources
- View the [DataDog Analytics](https://app.datadoghq.com/dashboard/6de-7vb-idu/shopify-monitor)
